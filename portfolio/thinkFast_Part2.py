"""@file thinkFast_Part2.py 
@brief This file contains a python script to use an IC and OC callback function tied to the LED and User button.
@details    Using the Nucleo board and the built-in Button and LED, this following program sets up an IC and OC callback
			function to control when LED turns on and when input capture occurs. The reaction time is calculated through comparing
			values captured by the callback functions.
@author Alyssa Liu
@date May 2, 2021
"""

import utime
import micropython
import pyb
import random

## Emergency buffer to store errors thrown inside ISR
micropython.alloc_emergency_exception_buf(100)

## Timer Object configured to be a counter
timer2 = Timer(2, period=0x7FFFFFFF, prescaler= 79)

## Timer 1 Channel - Set up for Output Compare connected to Pin A5
timer2_ch1 = timer2.channel(1, pin=Pin.cpu.A5, mode=Timer.OC_FORCED_INACTIVE, polarity=Timer.HIGH, callback=None)
## Timer 2 Channel - Set up for Input Compare connected to Pin B3 (connected to C13)
timer2_ch2 = timer2.channel(2, pin=Pin.cpu.B3, mode=Timer.IC,	callback=IC_CB, polarity=Timer.FALLING)

## Initalize input button
input_button = pyb.Pin(pyb.Pin.cpu.B3, mode=pyb.Pin.IN)

## Initalize LED button
LED_output = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT)

## Initalize LED boolean variable to know status
LED = False

## Initalize early_input boolean variable to trigger if user inputs before LED turns on
early_input = False

## Initalize button_input boolean variable to trigger when user inputs
button_input = False

reactTime = 0x7FFFFFFF

## Initalize empty list to hold values
reactRecords = []

## Set time_start variable equal to 0 to begin
time_start = 0

## Set last_compare variable to 0 at init
last_compare = 0

## Set last_cb_time variable to 0 at init
last_cb_time = 0

def OC_CB(Timer):
	""" @brief This function creates an Output Capture Callbuck and triggers LED to turn on and off.
        @details    Function uses global variables to turn LED on and off using the timer2 channel 1. 
        			The function also sets the last_compare variable based on if the button_input variable is true 
        			and/or LED is on.

        @param Timer timer object counter
    """
	global last_compare
	global LED
	global button_input
	global last_compare
	global delay
	
	print("in OC_CB \n")

	if (button_input):
		## Set LED status to off
		LED = False
		last_compare = timer2_ch1.compare() 
		last_compare += delay*1000   
		timer2_ch1.compare(last_compare)
	else:
		## If LED is not pressed during light blink duration and it's on
		if (LED): 
			## LED turns off
			LED = False
			last_compare = timer2_ch1.compare()
			## Add interval to the most recent compare value
			last_compare += delay*1000   
			##last_compare &= 0x7FFFFFFF
			timer2_ch1.compare(last_compare)
		else:
			## The LED is off, so it needs to be turned on
			LED = True
			last_compare = timer2_ch1.compare()
			## Add interval to the most recent compare value
			last_compare += 10**6  
			##last_compare &= 0x7FFFFFFF
			timer2_ch1.compare(last_compare)

def IC_CB(Timer):
	""" @brief This function creates an Input Capture Callbuck and triggers when user presses button.
        @details	Function uses global variables to set last_cb_time equal to current capture time. Like part A, it
        			also checks delay variable to see if user pressed button before LED turned on if so the function
        			sets early_input True. Regardless, the button_input boolean variable is set to True 

        @param Timer object counter

    """
	global last_cb_time
	global button_input
	global early_input
	global time_start
	
	print("in IC_CB \n")
	
	if(delay > utime.ticks_diff(utime.ticks_us(),time_start)):
		early_input = True
	button_input = True
	last_cb_time = Timer.channel(2).capture()

def calculate_react_time(reactRecords):
	""" @brief This function calculates reaction time based on last_compare and last_cb_time variables
        @details	The function calculates the reactionTime based on last_cb_time and last_compare recorded from
        			IC_CB and OC_CB respectively. Then the value is converted from ticks to a time scale. The reactionTime
        			is then appended to reactRecords list.
        @param reactRecords - list variable 
    """
	global last_compare
	global last_cb_time 

	reactionTime = (last_cb_time - last_compare)*(79/(8*10000000))
	reactRecords.append(reactionTime)


try:
	timer2_ch1 = timer2.channel(1, pin=Pin.cpu.A5, mode=Timer.OC_TOGGLE, polarity=Timer.HIGH, callback=OC_CB, compare=(timer2.counter() + 2000000)&0x7FFFFFFF)
	timer2_ch2.callback(IC_CB)

	while True:
		## Ensure that global variables are set properly (at reset position)
		reactTime = 0x7FFFFFFF
		button_input = False
		early_input = False
	
		## Record start time
		time_start = utime.ticks_us()

		## sets delay to be a random time between 2-3 seconds
		delay = random.randint(2000000, 3000000)

		pyb.delay(delay+1000)
		## If ISR triggered, button_input set to true.
		if (button_input == True):
			## If early_input is True, then user pressed button too early & time is not recorded
			if (early_input == True):	
				print("Button Pressed too early -- no time recorded \n")
			## Else, the reactTime is appended to list and printed out
			else:
				calculate_react_time(reactRecords)
				print('Nice! Reaction Time recorded:{:.2f} ms'.format(float(reactTime/1000))+'\n')
		
		## If ISR is not triggered, then list appends 1 second.
		else:
			reactRecords.append(1000000)
			print('Yikes! Too slow... Reaction Time recorded:{:.2f} ms'.format(float(reactTime/1000))+'\n')
			pass

except KeyboardInterrupt:	
	timer2.deinit()
	pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)
	
	## Create new list to removed any values that are recorded as 0x7FFFFFFF
	outputRecord = []
	for each in reactRecords:
		if each != 0x7FFFFFFF:
			outputRecord.append(each)
	## As long as list is greater than 0, calculate average and print out
	if len(outputRecord) > 0:
		average_reaction = (sum(outputRecord)/len(outputRecord))
		print('Average reaction time in microseconds: ', str(average_reaction))
		print('Average reaction time in milliseconds: {:3.1f}'.format(average_reaction/10**3))
		print('Average reaction time in seconds: {:1.4f}'.format(average_reaction/10**6))

	else:
		print("No Reactions Recorded")