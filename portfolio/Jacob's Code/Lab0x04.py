'''
@file Lab0x04

@page Lab0x04

@section sec_title4 Lab0x04: Hot or Not?

@author Jacob Burghgraef

@date May 13th, 2021

@section sec_overview Overview

This lab involves interfacing with an MCP9808 temperature sensor, collecting 
data from it for about 8 hours, and exporting the data to a csv. The recorded 
data can be seen in the plot below. For more information regarding 
documentation of the source code, please reference the following files: 
Lab4main.py and mcp9808.py \ref

@image html "TempDataPlot.JPG"
The data in the plot above was taken overnight from about 10 pm to 7 am on my 
desk in my bedroom.
'''