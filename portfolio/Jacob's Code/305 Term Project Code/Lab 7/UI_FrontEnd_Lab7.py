'''
@file UI_FrontEnd_Lab7.py
@package Lab_7
@brief Creates a UI for Reading and Plotting Motor Data in Spyder
@details This interface sets up the serial port, opens and resamples the given
         tracking data from the reference csv at 1/20th the sample rate (to 
         accomodate the nucleo's memory limits), sends that data to the nucleo,
         extracts the collected data from the nucleo and plots it.
@author Jacob Burghgraef
@date December 4, 2020

Note: I was unable to get an adequate plot for this lab, I would have liked to
continue debugging this lab, but I ran out of time. I believe most of this code
should work and is conceptually sound, so I'm not entirely sure where 
it all went wrong. I have sneaking suspicions that my encoder class is to 
blame for the excessively high J value of about 1*10^7, which is likely due to 
some error I was unable to find in my calculation of the delta value. 
Furthermore, when I looked through the data I was collecting in Putty, not 
only were my positions off, but the measured velocity (in rpms) was also quite
off from the reference value. This could be due to the KP values I was sending,
but I feel like it's more likely due to some error I was unable to catch in my
motor driver and encoder driver. I've spent the better part of the last week 
trying to test these classes, but I still can't find the problem. Additionally,
I've gone to office hours multiple times, and we were unable to find what the 
issue was. To reiterate, I believe the issue is with my math somewhere deep in
my encoder driver class, and also possibly in my closed loop class.

The source code can be found at: https://bitbucket.org/jburghgraef/me305_labs/src/master/Lab%207/UI_FrontEnd_Lab7.py
'''

import serial
import matplotlib.pyplot as plt
import time as frontend
    
## The serial object used to communicate with the Nucleo in Spyder
ser = serial.Serial(port='COM4',baudrate=115273,timeout=1)

# Blank lists for storing the reference values
time = []
velocity = []
position = []

## The kp value to be sent to the Nucleo
kp = 0

## The list of measured timestamps
timestamp = []

## The list of measured velocity values
omega = []

## The list of measured theta values
theta = []

# Opens the reference file
ref = open('reference.csv');

# Read data indefinitely. Loop will break out when the file is done.

while True:
    # Read a line of data. It should be in the format 't,v,x\n' but when the
    # file runs out of lines the lines will return as empty strings, i.e. ''
    line = ref.readline()
    
    # If the line is empty, there are no more rows so exit the loop
    if line == '':
        break
    
    # If the line is not empty, strip special characters, split on commas, and
    # then append each value to its list.
    else:
        (t,v,x) = line.strip().split(',');
        time.append(float(t))
        velocity.append(float(v))
        position.append(float(x))

# Can't forget to close the serial port
ref.close()

# Resample the data at 20 ms intervals instead of 1 ms intervals

## The number of data points from the reference csv
size = len(time)

# Lists to store the data resampling
tim = []
vel = []
pos = []

print('Loading csv values onto Nucleo...')

# Resample every 20th data point and send the values to the nucleo
# This loop buffers each iteration by a few microseconds to ensure that the 
# frontend doesn't send data too fast such that the backend can't read the data.
for i in range(0, size, 20):
    # Get the resampled point from the time array
    t = time[i]
    # Add that timestamp into the resampled list
    tim.append(t) 
    
    # Get the resampled point from the velocity array
    v = velocity[i]
    # Add that velocity value into the resampled list
    vel.append(v)
    
    # Get the resampled point from the position array
    p = position[i]
    # Add that position value into the resampled list
    pos.append(p)
    
    # Send the reference values to the serial port
    ser.write('{:}, {:}\r\n'.format (str(v).encode('ascii'), str(p).encode('ascii')))
    
    # Delay 20 microseconds to let the Nucleo catch up to the frontend
    frontend.sleep(2*1e-5)

print('Reference Values loaded onto Nucleo.')

# plot the data
plt.figure(1)

plt.title('Data read from the reference csv')

# Velocity first
plt.subplot(2,1,1)
plt.plot(tim,vel)
plt.ylabel('Velocity [RPM]')

# Then position
plt.subplot(2,1,2)
plt.plot(tim,pos)
plt.xlabel('Time [s]')
plt.ylabel('Position [deg]')

def sendKp():
        '''
        @brief Sends an ascii character to the Nucleo
        @return myval An integer representing the ascii character
        '''
        kp = input('Send a Kp value: ')
        ser.write(str(kp).encode('ascii'))
        myval = ser.readline().decode('ascii')
        return myval
   
def ExtractDataPacket():
        '''
        @brief Extracts position and timestamp values from Nucleo
        @details This uses the serial package to read the data written to the
                 local COM port from the nucleo. It then strips each line of
                  values and partitions them into a corresponding position, 
                  time, and omega arrays as integers. Finally, the last value
                  sent from the nucleo, the performance metric, is read and 
                  printed to the REPL for the user's convenience.
        '''
        n = 0
        
        ## The number of lines coming from the serial port
        size = 752
        
        # Wait 16 seconds to collect data
        frontend.sleep(16)
        
        for n in range(size-1):
            ## The string representing a single row of data from the data collection task
            line_string = ser.readline().decode('ascii')
            
            ## A list representing the extracted strings of the collected data
            line_list = line_string.strip().split(',')
            
            # Add the integer timestamp value to the timestamp array
            timestamp.append(int(line_list[0])/1e6)
            
            # Add the integer velocity value to the omega array
            omega.append(line_list[1])
            
            # Add the integer position value to the theta array
            theta.append(line_list[2])
            
        # Read the line containing the J value from the serial port
        j_line = ser.readline().decode('ascii')
        j = j_line.strip()
        print('The J value is: ' + str(j))
            
# Send The KP value
sendKp()
            
# Extract the data the Collection task wrote to the serial port
ExtractDataPacket()

# plot the data
plt.figure(2)

plt.title('Data collected from Nucleo with kp value: ' + str(kp))

# Velocity first
plt.subplot(2,1,1)
plt.plot(timestamp,omega)
plt.ylabel('Velocity [RPM]')

# Then position
plt.subplot(2,1,2)
plt.plot(timestamp,theta)
plt.xlabel('Time [s]')
plt.ylabel('Position [deg]')

# Close the Serial Port
ser.close()