"""
@file TPCalibration.py

@subpage ME405TPC Calibration

@section sec_titleTPC Touch Panel Calibration

@author Jacob Burghgraef and Alyssa Liu

@date June 11th, 2021

@section sec_overview Overview

@image html "Touch Panel Calibration Snippet.JPG"

The output shown above displays the 3 features of touch panel calibration:
    1. Scan Time Measurement: Each scan function is run 100 times and the 
    execution time for each run is averaged, yielding a good estimate of 
    average scan time. This is performed for the Xscan(), Yscan(), Zscan(), 
    and OptScan() functions, and the overall scan time - the time taken to 
    scan X, Y, and Z - for each scan method is displayed for comparison. The 
    total scan time is the sum of average scan times for the X, Y and Z scans, 
    whereas the optimal scan time is the total scan time of the optimized scan 
    function. Numerous runs of this calibration code has suggested that the 
    execution time for the optimal scan function is between 690 to 740 
    microseconds, which is 30% to 40% faster than the total scan time, which 
    is between 1050 to 1130 microseconds. The range of values is likely due to 
    the lack of reliability of the hardware.
    2. Effective touch panel height, width and center coordinates: The 
    physical height and width of the touch panel were 0.107 m and 0.187 m 
    respectively, but the touch panel doesn’t register these values when 
    measurements are taken. Using this calibration code numerous times has 
    suggested a zero error for all measurements of about .012 m in both the X 
    and Y axes, and the measured height and width are 0.078 m and 0.162 m 
    respectively. Moreover, the measured center point has been approximated to 
    be at about x = 0.09 m and y = 0.05 m when factoring in for the zero error. 
    This block of calibration code records these values by prompting the user 
    to touch the 4 corners of the touch panel and the approximate center of 
    the touch panel, with 3 seconds in between each data point recording to 
    allow the user time to move their finger. This data is then used to 
    approximate the height, width and center points. The measured center point 
    is the coordinate measured from the user touching at the center of the 
    touch panel, and the expected center point is calculated from the measured 
    height and width and zero error. Then, the percent difference is 
    calculated to provide a sanity check for both the measured and expected 
    values; the values should differ by less than 10% each time. This 
    calibration test should be run many times - at least five to ten times - 
    to get a good estimation of the touch panel’s height, width and center. 
    There is a bug with this function that outputs anomalous data in which x 
    coordinates recorded in this function (namely in position 4) will read as 
    if there was nothing in contact with the touch panel, despite the y and z 
    values suggesting otherwise. This is likely a hardware issue or some quirk 
    of micropython because numerous standard scan tests have indicated the x 
    coordinates work normally at this location when the scan is not called 
    from this function.
    3. Indefinite scanning at 5 second intervals: The optimized scan function 
    is called in a while True loop every 5 seconds to allow the user to record 
    data for as long as they want.
    
@section sec_note Notes
This code is located in the TouchPanelDriver.py file and the TouchPanelTest.py 
file, but the test file is more user-friendly and should be used for touch 
panel calibration. However, if the test file doesn’t work with the user’s 
hardware, then the driver file has a more simplified version with all the same 
basic functionality. For more information, please see the TouchPanelDriver.py 
and TouchPanelTest.py files. \ref

@section sec_file File List
TouchPanelTest.py \ref
TouchPanelDriver.py \ref
"""