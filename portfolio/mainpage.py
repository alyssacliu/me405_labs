## @file mainpage.py
#  Documentation for / use of mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_port Portfolio Details
#  Hello! Welcome to my ME405 Portfolio. 
#
#  Pages:
#  - @ref lab0x01
#  - @ref lab0x02
#  - @ref lab0x03
#  - @ref lab0x04
#  - @ref hw0x02
#  - @ref hw0x04
#  - @ref hw0x05
#  - @ref TermProject
#
#  @section sec_lab0 Lab0x00 Documentation
#  * Source: https://bitbucket.org/alyssacliu/me405_labs/src/master/Lab0x00/
#
#  @section sec_lab1 Lab0x01 Documentation 
#  * Source: https://bitbucket.org/alyssacliu/me405_labs/src/master/Lab0x01/vendingMachine.py
#  * Documentation: vendingMachine.py \ref
#
#  @section sec_lab2a Lab0x02a Documentation 
#  * Source - Part A: https://bitbucket.org/alyssacliu/me405_labs/src/master/Lab0x02/thinkFast.py
#  * Documentation - Part A: thinkFast.py \ref
#  * Video Demo: https://vimeo.com/544136523
#
#  @section sec_lab2b Lab0x02b Documentation
#  * Source - Part B: https://bitbucket.org/alyssacliu/me405_labs/src/master/Lab0x02/thinkFast_Part2.py
#  * Documentation - Part B: thinkFast_Part2.py \ref 
#  * Video Demo: https://vimeo.com/544136523
#
#  @section sec_lab3 Lab0x03 Documentation 
#  * Source: https://bitbucket.org/alyssacliu/me405_labs/src/master/Lab0x03/
#  * Documentation: adc.py \ref lab3_ui.py \ref
#
#  @section sec_lab4 Lab0x04 Documentation 
#  * Source: https://bitbucket.org/alyssacliu/me405_labs/src/master/Lab0x04/
#  * Documentation: lab4_main.py \ref MCP9808.py \ref
#
#  @section sec_hw2 HW0x02 Documentation 
#  * @ref hw0x02
#  * Hand calculations: https://nicho755.bitbucket.io/page1.html
#
#  @section sec_hw4 HW0x04 Documentation
#  * @ref hw0x04
#
#  @section sec_hw5 HW0x05 Documentation
#  * @ref hw0x05
#
#  @section sec_termProject_doc Term Project Documentation
#  * @ref TermProject
#  * Source: https://bitbucket.org/alyssacliu/me405_termproject/src/master/
#
#  @author Alyssa Liu
#
#  @date April 20, 2021
#
#  @page lab0x01 Lab 0x01: Virtual Vending Machine
#
#  @section sec_lab0x01_summary Summary
#  In this lab, we were required to utilize the finite-state machine design to create a virtual vending machine. 
#  The vending machine can take in a variety of inputs, such as accepting and calculating inserted money, 
#  updating and returning current balances, and dispensing selected drinks. As stated in the lab requirements, 
#  the machine runs without blocking commands. 
# 
#  <b> Source Code can be found here:</b> https://bitbucket.org/alyssacliu/me405_labs/src/master/Lab0x01/vendingMachine.py
#
#  @section sec_lab0x01_engineering_requirements Engineering Requirements
#  The vending machine has four input buttons representing four different beverages that users can select from with 
#  corresponding prices:
#  1) Cuke = $1.00
#  2) Popsi = $1.20
#  3) Spryte = $0.85
#  4) Dr. Pupper = $1.10
#  
#  In addition, the machine take in user input in the form of money being inserted through the "Insert Coin" slot
#  and requesting for the current balance to be returned via the eject button. The machine will output strings on 
#  the LCD screen and display current balance, insufficient funds, drink output confirmation, and amount of change returned. 
#
#  To ensure the machine runs like a finite state machine, the script cannot include any blocking, or input, commands, 
#  the program must account for the following behavior:
#  - On startup: Vendotron displays an initialization message on the LCD screen.
#  - At any time a coin may be inserted: When a coin is inserted, the balance displayed
#    on the screen must reflect so.
#  - At any time, a drink may be selected:
#    -# If the current balance is sufficient, Vendotron vends the desired beverage through the flap at the bottom of the 
#       machine and then correct change is provided through the coin return.
#    -# If the current balance is insufficient then the machine displays an Insufficient Funds
#       message and then displays the price for the selected item.
#  - At any time the Eject button may be pressed: When the eject button is pressed,
#    the machine returns the full balance through the coin return. The balance should
#  contain change in the least number of denominations. 
#  - The vending machine must prompt the user to select a second beverage if there is remaining balance.
#
#  @section sec_lab0x01_vending_machine_FSM Vending Machine Finite State Machine Diagram
#  Below is the finite state machine diagram for the vending machine.
#  (Adapted from my HW 0x00 submission)
#  @image html VM_FSM.png "Figure 1. Vendotron Finite-State Transition Diagram"
#  
# @section sec_lab0x01_user_interaction User Interaction
#  Users are able to input the following valid commands via keyboard \n
#  <b> Payment Input </b> \n
#  '0' - insert 1 cent \n
#  '1' - insert 5 cents \n
#  '2' - insert 10 cents \n
#  '3' - insert 25 cents \n
#  '4' - insert 1 dollar \n
#  '5' - insert 5 dollars \n
#  '6' - insert 10 dollars \n
#  '7' - insert 20 dollars \n
#
#  <b> Selecting Drink: </b> \n
#  'C' or 'c' - select Cuke \n
#  'P' or 'p' - select Popsi \n
#  'S' or 's' - select Spryte \n
#  'D' or 'd' - select Dr. Pupper \n
#
#  <b> Return Balance </b> \n
#  'E' or 'e' - eject change \n
#
#  @page lab0x02 Lab 0x02: Think Fast
#
#  @section sec_lab0x02_summary Summary
#  This lab was a two part assignement. For the part A, we were required to build a reaction time based game using the 
#  LED and User Button on the Nucelo board. The User Button would trigger an ISR function in our code to perform certain actions
#  to allow us to record when the button is pressed in relation to the LED turning on. For part B, we took this game a step further
#  and create an Input Capture (IC) and Output Capture (OC) function to interact with the button and LED respectively. 
# 
#  <b> Source Code for part A can be found here:</b> https://bitbucket.org/alyssacliu/me405_labs/src/master/Lab0x02/thinkFast.py
#  <b> Source Code for part B can be found here:</b> https://bitbucket.org/alyssacliu/me405_labs/src/master/Lab0x02/thinkFast_Part2.py
#  
#  @section sec_lab0x01_engineering_requirements Engineering Requirements
#  For Part A, we were required to use a Nucleo and configure our python script to interact with the LED (A5) and User Input Button (C13)
#  The program uses an ISR callback function, that we set to run whenever the button is on its falling edge, therefore, whenever the user presses
#  the button. This ISR will set variables and preforms other actions without returning anything. The python script will then
#  records the reaction times of the users as they were required to press the button as soon as they saw the LED turn on. 
#  
#  Assignment Guidelines for Part A:
#  * Wait a random time between 2 and 3 seconds, then turn on the little green LED connected to microcontroller pin PA5
#  * Record a timestamp in microseconds using utime.ticks us(). 
#  * The LED stays on for one second. 
#  * An external interrupt should be set up on pin PC13, which is connected to the blue User button on the Nucleo. 
#  * When the button is pressed, it causes a falling edge on the pin, so an interrupt should be triggered by the falling edge
#  * The interrupt callback function should read a value from utime.ticks us() to see how many microseconds have elapsed since the LED was turned on. 
#  * Use utime.ticks diff() to find the relative amount of time between the timestamps instead of using traditional arithmetic.
#  * The process of turning the LED off and on and measuring reaction times should repeat until the user presses Ctrl-C to stop the program. 
#  * When the program is stopped, the average reaction time over the number of tries should be calculated and displayed. 
#  * An error message should be printed if no reaction time was successfully measured (such as if the button wasn’t pressed)
#  * Not crash just because the user didn’t use it correctly.
#
#  For Part B, we were asked to improve our code by using Input Capture (IC) and Output Capture (OC) functions.  
#  We connected the pins C13 and B3 to trigger an IC callback rather than external interrupt.
#  
#  Requirements:
#  * Configure Timer 2 as a free-running counter with an appropriate prescaler and period to measure times between zero and a few seconds. Now, write two callback functions associated with two channels on Timer 2.
#  * The first callback functions hould be triggered by an Output Compare (OC) compare match on PA5 using Channel 1 of Timer 2. 
#  * Configure this interrupt so that PA5 goes high when the interrupt occurs. 
#  * Within the interrupt, reconfigure the timer to trigger the same interrupt, but this time setting PA5 low after 1 second.
#  * The second callback function should be triggered by Input Capture (IC) on PB3 us- ing Channel 2 of Timer 2. 
#  * When the IC interrupt is triggered, the timer value will automatically be stored in the timer channels capture value. 
#  * The stored value can be accessed using the timer function called timerchannel.capture().
#
#  @page lab0x03 Lab 0x03: Pushing the Right Buttons
#
#  @section sec_lab0x03_summary Summary
#  In this lab, we were required to create a tool to collect, store, and plot data from the user button on the Nucleo. We looked to measure the step response of the RC circuit connected to the USER button on the Nucleo. 
#  The pin representing the state of the USER button, Pin C13, is shorted to a pin that accepts analog input, Pin A0. We used the ADC class in the pyb module is used to configure Pin A0 to read the voltage output from the 12-bit 
#  Successive Approximation Register (SAR) on the Nucleo. This voltage output is in the form of a discrete integer between 0 and 4095, and represents a fraction of the DC power supply voltage (3.3 V). This output is converted 
#  to units of Volts in the frontend using (output)*(3.3 V)/4095. Finally, the voltage is plotted against time in milliseconds to yield a step response with a time constant of approximately 0.5 ms. Additionally, the data 
#  (timestamp and voltage integers) are exported as a csv for the convenience of further data analysis. Finally, the timestamps are estimated for the voltage data by assuming each voltage data point is sampled exactly 20 microseconds apart. 
#  The data buffer array storing the voltage integers samples data at 50kHz to get about 500 data points, which corresponds to about 5 ms of data - plenty of time to measure the step response, which reaches steady state in 
#  approximately 4 to 5 times the time constant, or about 2 to 2.5 ms. 
#  Source Code and CSV file can be found here: https://bitbucket.org/alyssacliu/me405_labs/src/master/Lab0x03/
#
#  @section sec_lab0x03_engineering_requirements Engineering Requirements
#  Front End User Interface Requirements:
#  * Build a front end User Interface
#  * Runs on computer and recieves user input
#  * If user presses 'G', the Nucleo waits for user input (button press)
#  * After data collect, script outputs plot and .csv file
#  
#  @section sec_lab0x03_data Data Collection
#  @image html lab3_voltage_time_plot.png "Figure 1. Voltage vs Time Plot (in collaboration with lab partner - Jacob Burghgraef)"
#
#  Figure 1 was generated by a matplotlib module by my Lab partener to illustrate the voltage integer data converted to units of volts on the y-axis and the respective timestamp on the x-axis.
#  (There is a 3 second shift due to the delays between the collect start time and when the user input is captured)
#
#  @image html lab3_LinearVT_plot.png "Figure 2. Linearized Voltage vs Time Plot (in collaboration with lab partner - Jacob Burghgraef)"
#
#  Figure 2 was created in execl to represent the first order DE from the capacitor in the RC circuit. 
#
#  <b> Discussion: </b> Based on Figure 2, which has the slope of the best fit line equalling 2.0. We are able to find that the experimental time constant is 0.498 ms. 
#  as a result, given the theoretical time constant being 0.48 ms, the error percentage fall around 3.6%. We found that there are 3 valid methods to approximate this time constant:
#  find the value of the time constant at 62% of 3.3V, find the tangent line at the beginning of the step response and the time in which the line crosses 3.3V, and linearize the DE to 
#  obtain the best fit.
#
#  @page lab0x04 Lab 0x04: Temperature Sensor 
#
#  @section sec_lab0x04_summary Summary
#  Through this lab, we were introduced to sensors and how they interface with microcontrollers using digital connections. In this case, we used an I2C interface.
#  In the assignment, we created and tested a driver for an I2C-connected sensor and used it to log data to be placed into an excel sheet. We wrote a main.py and mcp9808 module 
#  that ran for a period of 8 hours and stored data into a CSV file. Using the CSV file, we generated a plot. 
#  
#  Note: This assignment was completed in collaboration with Jacob B. However, we did have different coding scripts.
#  Alyssa's Source Code: https://bitbucket.org/alyssacliu/me405_labs/src/master/Lab0x04/
#  Jacob's Source Code: https://bitbucket.org/jburghgraef/me405_labs/src/master/Lab%204/
#
#  @section sec_lab0x04_data Data and Plot
#
#  <b> The program was run from 9:00pm to 5:00am on my desk. Please note, I live in a renovated garage and the temperature
#  tends to flucuate.</b>
#
#  Outputted CSV File can be found here: https://bitbucket.org/alyssacliu/me405_labs/src/master/Lab0x04/temperature_data.csv
#  
#  @image html lab0x04_tempcompare.png "Figure 1. Temperature Plot based on Data Gathered: Comparing Temperature of Board vs Temperature of Sensor"
#
#  @image html lab0x04_tempsensorplot.png "Figure 2. Temperature Plot based on Data Gathered: Temperature of Sensor vs Time"
#
#  @page hw0x02 HW 0x02: Term Project System Modeling
#
#  @section sec_hw0x02_summary Summary
#  In this assignment, we were required to analyze and derive calculations from a given 4 body linkage. With the final goal being to relate motor
#  speed to the tilt speed of the platform. 
#  
#  @section Hand Calculations
#  This assignment was completed in collaboration with Nicholas Greco. Please note that Nick did the majority of the work as I am a CPE and could not contribute
#  anything major to this intermediate dynamic based assignment. 
#  <b> Hand Calculations can be found here:</b> https://nicho755.bitbucket.io/page1.html
#
#  @page hw0x04 HW 0x04: Simulation or Reality?
#  
#  @section sec_hw0x04_summary Summary
#  Building off of HW 0x02, we are furthering our preparation for our term project by developing a simulation and test a controller for the simplified
#  model of the pivoting platform. Our group decided to attempt both python and matlab to complete this assignment, with the resulting and properly functioning
#  submission to be in matlab, which you will find below. However, you will find our python script linked below as well. 
#  
#  <b> Source code - Python:</b> https://bitbucket.org/alyssacliu/me405_labs/src/master/HW%200x04/hw4.py
# 
#  @section sec_hw0x04_parameters Numerical Parameters
#  In our simuation, we used the following parameters:
#
#  @image html hw0x04Parameters.png "Figure 1. Provided numerical parameters for the simulation"
# 
#  We did not need to design our own controller for this assignment, rather we were given the following gains to test our system performance in closed loop:
#  K = [-0.3, -0.2, -0.05, -0.02] with the units K = [N, N*m, N*s, N*m*s]
#
#  @section sec_hw0x04_engineering_requirments Engineering Requirements
#  - Use the linearization of our system model and express it in state-space form
#  - Using software, implement model that can be simulated in open-loop given a prescribed set of inital conditions
#  - Run simulations in open-loop and generate series of plots showing the dynamic response for each state variable
#  - <b> Simulation A:</b> The ball is initially at rest on a level platform directly above the center of gravity of the platform and 
#    there is no torque input from the motor. Run this simulation for 1 [s].
#  - <b> Simulation B:</b> The ball is initially at rest on a level platform offset horizontally from the center of gravity of the 
#    platform by 5 [cm] and there is no torque input from the motor. Run this simulation for 0.4 [s].
#  - <b> Simulation C:</b> Test simulation in closed-loop by implementing a regulator using full state feedback: u = - Kx and run simulation for at
#    least 20 [s]. 
#
#  @section sec_hw0x04_matlab_simlink Matlab and Simulink Documentation
#  <b> Matlab Script:</b>
#  @image html HW4_SystemParameters.png "Figure 2. System Parameters Matlab Script"
#
#  @image html HW4_SettinguptheMatrices.png "Figure 3. Setting up the Matrices Matlab Script"
#
#  @image html HW4_PlottingResults.png "Figure 4. Plotting Results Matlab Script"
#
#  @image html HW4_DeterminingtheInverse.png "Figure 5. Determining the Inverse Matlab Script"
#
#  @image html HW4_OpenLoopModelCaseA_1.png
#  @image html HW4_OpenLoopModelCaseA_2.png "Figure 6. Simulation/Case A Open Loop Matlab Script"
#
#  @image html HW4_OpenLoopModelCaseB_1.png 
#  @image html HW4_OpenLoopModelCaseB_2.png "Figure 7. Simulation/Case B Open Loop Matlab Script"
#
#  @image html HW4_ClosedLoopModel_1.png 
#  @image html HW4_ClosedLoopModel_2.png "Figure 8. Simulation/Case C Closed Loop using Case B Test Conditions Matlab Script"
#
#  <b> Simulink:</b>
#
#  @image html HW4_SimulinkModel.png "Figure 9. Simulink Model"
#
#  @section sec_hw0x04_plots Simulation Plots
#  <b> [Simulation A] </b>
#  @image html HW4_OL_Plot_PartA.jpeg "Figure 10. Subplots for Simulation/Case A Open Loop"
#
#  <b> Discussion: </b> The ball is initially placed at the center of the platform, the one stable point in our inherently unstable system. 
#  Given that there are no appreciable forces acting on the ball in this simulation, no initial displacements/velocities of the platform or ball, 
#  and no torque input from the motor, this system will not deviate from its initial stable point. Therefore, this is a static system, and 
#  such behavior is shown by the zero slope lines in all displacement and velocity plots.
#  
#  <b> [Simulation B] </b>
#  @image html HW4_OL_Plot_PartB.jpeg "Figure 11. Subplots for Simulation/Case B Open Loop"
#
#  <b> Discussion: </b> The position of the ball as shown in plot [1,1] shows an interesting tendency to climb the incline, moving a direction opposite the tilt. 
#  Given that no force is being put into the ball, this tendency is hard to explain. Using newton’s 2nd law it is conceivable that the ball has an small nonzero 
#  acceleration as the platform tilts that acts in a direction opposite the motion of the platform. This is confirmed when we look at the velocity graph, whose 
#  change with respect to time is distinctly nonlinear and negative, meaning there is a nonzero, negative acceleration in the direction opposite the rotation of the ball. 
#  Frictional effects not being appropriately modeled might also have something to do with this physically inconceivable upward motion.
#  
#  With the oddity explained, the rest of the ball’s position and velocity graph’s show motion we would expect of an open loop controller. The platform will continue to tilt 
#  to greater and greater angles [plot[1,2]] at a higher and higher rate [plot [2,2]], increasing towards instability, the point at which the ball falls off the platform.
#
#  <b> [Simulation C] </b>
#  @image html HW4_CL_Plot.jpeg "Figure 12. Subplots for Simulation/Case C Closed Loop"
#
#  <b> Discussion: </b> As plot[1,1] shows, the ball starts at a position of x = 0.05m, which drives the plate to tilt clockwise about the y-axis, as shown in plot[1, 2]. 
#  The system’s regulator detects this initial displacement and attempts to correct the displacement by driving the motor to oppose the ball’s displacement. 
#  As a result, even though the platform’s angular velocity starts negative [clockwise] owing to the ball’s initial displacement [plot [2,2]], the clockwise 
#  angular velocity slowly decreases as the torque from the motor opposes the ball’s translation and, by association, the clockwise angular displacement of the platform as well. 
#  The effect on the ball’s velocity is shown in plot[2,1], where the counteracting torque applied by the motor to the platform drives the ball’s position to change in the direction 
#  opposite its initial displacement.
#
#  The above description is for one cycle of the closed loop controller. Owing to mechanical losses and the corrective actions of the regulator, the peaks associated with each oscillation 
#  will gradually decrease until the ball is more or less returned to a stable state at the center of the platform, which occurs roughly 20 seconds after the initial linear displacement.
#
#  @page hw0x05 HW 0x05: Full State Feedback
#  
#  @section sec_hw0x05_summary Summary
#  In preparation of our term project and to continue to build off of HW 0x02 and HW 0x04, we developed a closed loop controller
#  for our system model. To ensure consistency and accuracy of our system, we decided to pursue the MatLab/Simulink tools in order to 
#  complete our assignment. You will find our Matlab and Simulink documentation below.
#
#  @section sec_hw0x05_engineering_requirements Engineering Requirements
#  
#  * Determine 1x4 gain vector K = [K1, K2, K3, K4]
#  * Define a method of determining pole locations based criteria
#  * Find the characteristic polynomial associated with selected pole locations
#  * Determine the gains K1 through K4.
#
#  Method of determining pole locations based on criteria was making a dominant pole approximation and choosing two large poles along with two 
#  time-domain characteristics like settling time and overshoot percentage. 
#
#  @section sec_hw0x05_Hand_Calculations Hand Calculations
#  @image html hw0x05_handcals_1.png
#  @image html hw0x05_handcals_2.png
#
#  @section sec_hw0x05_matlab_and_simulink Matlab and Simulink
#
#  <b> HW 0x05: Developing a Closed Loop Control System for the Balancing Platform</b>
#  
#  @image html hw0x05_systemparameters.png 
#  @image html hw0x05_1.png
#  @image html hw0x05_2.png
#  @image html hw0x05_3.png
#  @image html hw0x05_4.png
#  @image html hw0x05_6.png
#  @image html hw0x05_7.png
#
#  @image html HW4_SimulinkModel.png "Figure 1. Simulink Model"
#  
#  @section sec_hw0x05_Results Results
#  <b> Verify K values using place() command </b>
#  @image html hw0x05_verify.png "Figure 2. Compared calculated results versus results found via place() command"
#  
#  As you can see in Figure 1, the values we outputted matched that of the values resulting from the use of the place() command.
#
#  @image html HW0x04_Controller.jpeg "Figure 3. HW 0x04 Controller Plots"
#
#  @image html OurController.jpeg "Figure 3. HW 0x05 Controller Plots"
#  
#  @section sec_hw0x05_discussion Discussion & Analysis
#  <b> 1. Show confirmation that your calculated gains correctly place the closed-loop poles at your desired locations </b>
#  
#  Setting and iterating upon the parameters of rise time and percent overshoot, we arrive at gain values after solving the 
#  system characteristic polynomial for the values of "K". When using the place() function to compare the calculated gains 
#  to that calculated using the place() function (see attached MATLAB code), we see identical results between the two, 
#  with the values being: [-1.7159 -0.6088 -0.4783 -0.547].
#
#  <b> 2. Show your tuned closed-loop simulation results and comment on the differences (hopefully positive ones) compared to the
#  closed-loop response in HW 0x04. </b>
#  
#  From inspection, the tuned open-loop system has a smaller settling time, rise time and percent overshoot. Accomplishing such 
#  variations involved increasing the value of the damping ratio (zeta) which increased the value of the real term, driving percent 
#  overshoot and settling time down. Additionally, but to a lesser extent, the increase of the damping ratio and increase of the 
#  natural frequency drove the rise time up as well. These differences illustrate a more effective controller, one that reaches 
#  equilibrium sooner, that responds to the ball's placement quicker, and exhibits more controlled motion.
#
#  <b> 3. Comment on the performance of your system: Did you achieve the criteria that you selected? </b>
#  
#  In order to compare percent overshoot values between the HW0x04 controller and the HW0x05 controller design above, 
#  I used the min function to obtain minimum (negative) values as these correspond to the actual peak. The HW0x04 
#  controller exhibited a peak of -0.03048 [m], whereas the HW0x05 controller exhibited a peak of -0.0068 [m]. 
#  Given the large difference between the values and that they both share the same steady state value, the explicit 
#  OS comparison is unnecessary. Additionally, using the damping ratio and natural frequency values of the HW0x05 
#  controller, we get a settling time of 1.195 [s], which can be observed as being far less than that of the HW0x04 
#  controller. Finally, using peak time as opposed to rise time calculations because they both would exhibit similar 
#  trend (and for the sake of parsimony), we find the peak time of the HW0x05 controller to be 0.5638 [s], as compared 
#  to the 1.142 [s] value of the HW0x04 controller.
#
#  @page TermProject Final Term Project
#
#  @section sec_TermProject_summary Summary
#  Our final term project consisted of creating a mechanical closed loop system that would properly adjust positions via two DC motors to 
#  balance a weighted ball on a resistive touch panel. To properly pace ourselves through this project, we were recommended to complete 4 
#  components over the remainder of the quarter, which provided us with a week duration to complete each element of the system. 
#
#  The components were:
#
#  - Reading from Resistive Touch Panels
#  - Driving DC Motors with PWM and H-bridges
#  - Reading from Quadrature Encoders
#  - Balancing the Ball
#
#  Through using hardware shipped to our location, we soldered and assembled the system and began to intermittently develop our code.
#
#  @section sec_TermProject_navigation Navigation
#
#  In the pages below, you will find all related areas of our projects for all four components.
# 
#  1. @ref System_Calculations
#  2. @ref Project-SetUp
#  3. @ref Engineering-Requirements
#  4. @ref Software_Components_and_Documentation
#  5. @ref DC_Motor_Fault_Detection
#  6. @ref Calibration_and_Tuning
#  7. @ref Obstacles
#  8. @ref Controller_Task_Diagram
#  9. @ref Results
#  10. @ref Source_Code
#
#  @page System_Calculations Term Project: System Calculations
#  
#  You will find all related system calculations in the following file: EOMDerivation.py
#  
#  These calculations were derived throughout the quarter from our homework assignemnts.
#
#  @page Project-SetUp Term Project: Hardware Set Up
#
#  @section sec_resistiveTouch Resistive Touch Panel
#
#  Through the provided hardware, we were capable of utilizing a resistive touch panel that would allow us to write a program that scans 
#  the touch panel the position of any contact. Acting like a potentiometer, or voltage divider, we were able to scan for the X and Y positions 
#  through reading the voltages throughout the panel.
#
#  @image html RTP_Schematic.png "Figure 1. Schematic Representation of a 4-Wire Resistive Touch Panel"
#
#  We set Xp to 3.3V and Xm to 0V, which allowed the voltage at the junction to be measured by one of the Y components, with the other pin
#  acting as a floating pin. Through this design, we would be able to tell where the point of contact exists as the resistance of Xp would 
#  change and the measured voltage would increase or decrease proportionally. Similarly, as the voltage is consistent and uniform throughout the 
#  panel, then we can say the voltage scales linearly with position resulting in translation between voltage and position. 
#
#  @image html RTP_Pinout.png "Figure 2. Touch Panel Pinout"
#
#  To connect the resistive touch panel to the system, we utilized the FPC Breakout component that we soldered and connected to the touch panel. 
#  We then connected the sensor to the Nucleo via the breakout board headers via 4 pin extension cables.
#
#  @image html RTP_hardwareSetup.png "Figure 3. Touch Panel Hardware Setup"
#
#  @section sec_motors Motors
#
#  Our two motors were connected using the provided tables in our assignment documentation and through using supplementary datasheets for the DRV8847 Motors.
#
#  @image html DC_MotorConnections.png "Figure 4. Connection between the DRV8847 dual H-Bridge and the Nucleo"
#
#  @image html DC_4Pin_Config.png "Figure 5. 4-Pin Configuration truth table for the DRV8847 Motor Driver to drive two DC Motors with Pulse Width Modulation"
#
#  @section sec_encoder Encoder
#
#  We used the provided pinout table to understand how to connect the encoder and timer channels.
#
#  @image html encoder_pinout.png "Figure 6. Encoder Pinout Table"
#  
#  @page Engineering-Requirements Term Project: Engineering Requirements
#
#  @section sec_RTP Resistive Touch Panel Lab
#
#  - A constructor that requires input parameters for four arbitrary pin objects representing xp, xm, yp, and ym as well as additional numerical parameters 
#    representing the width and length of the resistive touch panel and the coordinate representing the center of the resistive touch panel.
#  - Three individual methods that scan the X, Y, and Z components respectively.
#     * The X and Y components should return values using the same units as the width and length specified in the constructor such that the readings are zero 
#       if the point of contact is in the middle of the touch panel. 
#     * The Z component should return as a Boolean value representing whether or not something is in contact with the touch panel.
#  - A method that reads all three components and returns their values as a tuple.
#  - You must be able to read from a single channel in less than 500μs and all three channels in less than 1500μs to guarantee that your driver works cooperatively with other 
#    code you will run for your project. If you optimize your code properly you should be able to get the time to measure all three channels down to less than 500μs.
#  - An optional, but very beneficial feature would be an additional method that allows you to calibrate your sensor by touching the screen in multiple locations and computing 
#    the horizontal and vertical scaling and center location automatically.
#  
#  @section sec_DCMotors DC Motors Driver Lab
#
#  - Define two classes: Motor Driver IC and Motor connected to driver IC
#  - Driver must independently interact with each motor
#  - Integrate Fault Detection Components (see requirements in Fault Detection Section)
#
#  @section sec_encoderDriver Encoder Driver Lab
#
#  - Write a class which encapsulates the operation of the timer to read from an encoder con- nected to arbitrary pins.
#  - A user should be able to create at least two objects of this class on different pins and different timers; ideally, the user would be able to specify any valid pin and timer 
#    combination to create many encoder objects. These two objects must be able to work at the same time without them interfering with one another.
#  - Your code must work if the timer overflows (counts above 216 − 1) or underflows (counts below zero).
#  - The position should count total movement and not reset for each revolution or when the timer overflows.
#
#  @page DC_Motor_Fault_Detection Term Project: DC Motor Fault Detection
#
#  You will find out Fault Detection program in DCMotorsFD.py
# 
#  @section sec_faultDetect_engr Engineering Requirements
#
#  * Trigger an external interrupt callback when the nFAULT pin goes low.
#  * As a result of the interrupt callback, all motor functionality must stop until you clear the fault with some form of deliberate user input.
#  * You must be able to clear the fault and resume operation without resetting the microcontroller.
#  
#  @section sec_solution Our Solution
#
#  Fault detection is first detected in the Motor Driver class DRV8847.py, which uses an external interrupt to raise a fault detection flag. This flag is then read as an attribute of the motor 
#  driver object instantiated in the motor task, where the nFAULT pin is activated when the flag is raised. As this nFAULT pin is activated, the DRV8847 motors go into a fault state where the motors 
#  are unable to be used until the nFAULT pin is deactivated. Back in the Motor Task class, when the nFAULT pin is activated, the motor task will raise a fault flag and then share this information with 
#  the other tasks using a shared variable in the TPShares.py file. 
#
#  The UserTask.py file runs at a higher frequency than all other tasks, so it should detect this fault flag first. Upon detection, the UserTask will lower the fault flag, the begin balancing flag and the 
#  all clear flag, and raise a halt flag using TPShares file. Upon the next iteration of the Encoder, Touch Panel, Controller, and Data Collection tasks, the halt flag will be detected, which will send these 
#  files back into an idle state, where they wait for another command from the UserTask before doing anything else. Additionally, the UserTask will transition to a fault handling state where instructions on 
#  how to clear the fault are conveyed to the user. After the user has cleared the fault, they will need to enter a command to the UserTask indicating that they have cleared the fault.
#
#  This next command is the ‘all clear’ command, which has a corresponding flag denoted by ‘allclr’; this flag is raised when the user enters an ‘a’ into the console on Putty. Upon receiving the all clear from 
#  the user, the allclr flag is read by the Motor Task and the Controller task, which transition out of their fault states. The motor task clears the fault using the clear_fault() function in the DRV8847 motor 
#  driver, which clears the fault flag in the Motor class and deactivates the nFAULT pin. The motor task then transitions back to its normal run state, but the motors won’t run until the system has been given 
#  the command to begin balancing again because the duty cycles are set to zero. The Encoder, Touch Panel, Controller and Data Collection tasks all immediately transition to an idle state where they wait for 
#  the ‘begin balancing’ command.
#
#  After the fault has been cleared, the system is effectively ‘reset’ to the initial idle state just after startup, where the user must pre-balance the platform and press ‘b’ to begin balancing the ball. Note 
#  that the option to force a fault state has been given to the user, where they can press ‘h’ to send the controller, encoder, touch panel, and data collection tasks to an idle state, and the UserTask will 
#  go into a fault detection state. However, the motors remain in the normal run state, but the duty cycle is set to zero.
#
#  @page Calibration_and_Tuning Term Project: Calibration and Tuning
#
#  @image html TouchPanelResults.png "Figure 1. Screenshot of our Touch Panel User Interface for Calibration"
#  
#  The output shown above displays the 3 features of touch panel calibration: 
#  
#  1. Scan Time Measurement: Each scan function is run 100 times and the execution time for each run is averaged, yielding a good estimate of average scan time. This is performed for the Xscan(), Yscan(), Zscan(), 
#  and OptScan() functions, and the overall scan time - the time taken to scan X, Y, and Z - for each scan method is displayed for comparison. The total scan time is the sum of average scan times for the X, Y and Z 
#  scans, whereas the optimal scan time is the total scan time of the optimized scan function. Numerous runs of this calibration code have suggested that the execution time for the optimal scan function is between 
#  690 to 740 microseconds, which is 30% to 40% faster than the total scan time, which is between 1050 to 1130 microseconds. The range of values is likely due to the lack of reliability of the hardware.
#  
#  2. Effective touch panel height, width and center coordinates: The physical height and width of the touch panel were 0.107 m and 0.187 m respectively, but the touch panel doesn’t register these values when measurements
#  are taken. Using this calibration code numerous times has suggested a zero error for all measurements of about .012 m in both the X and Y axes, and the measured height and width are 0.078 m and 0.162 m respectively. 
#  Moreover, the measured center point has been approximated to be at about x = 0.09 m and y = 0.05 m when factoring in for the zero error. This block of calibration code records these values by prompting the user to 
#  touch the 4 corners of the touch panel and the approximate center of the touch panel, with 3 seconds in between each data point recording to allow the user time to move their finger. This data is then used to 
#  approximate the height, width and center points. The measured center point is the coordinate measured from the user touching at the center of the touch panel, and the expected center point is calculated from the measured 
#  height and width and zero error. Then, the percent difference is calculated to provide a sanity check for both the measured and expected values; the values should differ by less than 10% each time. This calibration test
#  should be run many times - at least five to ten times - to get a good estimation of the touch panel’s height, width and center. There is a bug with this function that outputs anomalous data in which x coordinates recorded 
#  in this function (namely in position 4) will read as if there was nothing in contact with the touch panel, despite the y and z values suggesting otherwise. This is likely a hardware issue or some quirk of micropython 
#  because numerous standard scan tests have indicated the x coordinates work normally at this location when the scan is not called from this function.
#  
#  3. Indefinite scanning at 5 second intervals: The optimized scan function is called in a while True loop every 5 seconds to allow the user to record data for as long as they want.
#
#  Note this code is located in the TouchPanelDriver.py file and the TouchPanelTest.py file, but the test file is more user-friendly and should be used for touch panel calibration. However, if the test file doesn’t work 
#  with the user’s hardware, then the driver file has a more simplified version with all the same basic functionality. For more information, please see the TouchPanelDriver.py and TouchPanelTest.py files. \ref
#
#  @page Obstacles Term Project: Obstacles
#
#  Throughout this lab, we found that we had some issues with the hardware. For a period of time, we found that between the two of us, we had a malfunctioning touch panel and an incorrectly/missing component from our lab kit. 
#  Similarly, we also found that neither of our motors would properly adjust the plate due to the lack of friction and inability to properly tighten the motor arm components around the axles without stripping the screws. As a 
#  result, the motor arms would not interact with the motors properly and the plate would not make the correct and smooth adjustments.
#
#  @page Software_Components_and_Documentation Term Project: Software Components and Documentation
#
#  <b> Driver Files:</b>
#  
#  There were areas of our driver files that was contributed by ME 305 work. You will find our driver files below.
#  
#  - TouchPanelDriver.py 
#  - EncoderDriver.py 
#  - DRV8847.py 
#  
#  <b> Task Files:</b>
#  
#  For documentation and information on our Tasks please refer to our TasksOverview.py 
#  
#  <b> Other Project Files:</b>
#
#  - TermProjmain.py 
#  - Controller_Calcs.py 
#  - TouchPanelTest.py
#  - TPCalibration.py
#  - TPShares.py
#
#  @page Controller_Task_Diagram Term Project: Controller Task Diagram
#  
#  Our controller task is designed in a way that would allow us to seamlessly share variables throughout the program to ensure they are updated and adjusted as the data is collected and outputs are sent to the motors. 
#
#  @image html high_level_task.png "Figure 1. High Level Task Diagram"
#
#  To begin, there will be a user startup interaction on the user terminal or Putty. The controller task will read from the UI task, Touch Panel Task, and Encoder Task, in order to calculate the necessary values to share 
#  and send to the Motor Task. Simultaneously, all the data received is recorded via our Data Collection Task, which ultimately outputs a CSV file on our Nucleo.
#
#  You can find more information about the task and further documentation on our TasksOverview.py /ref file.
#
#  @page Results Term Project: Results
#
#  Unfortunately, we were unable to properly get many results due to the hardware issues we found when trying to test the motors. Despite the recommendations provided to use by Charlie
#  during office hours on Thursday, I was unsuccessful in running our hardware with our software. While Jacob was able to get his motors functioning, we ran out of time to debug and test
#  While it is a disappointing outcome, we did have a completed code set for the project, and with another couple days, I firmly believe we would have had a functioning project.
#
#  Our results are rather disappointing yet still encouraging, for we got the balancing platform to respond to the ball’s movement. However, the platform does not properly balance the 
#  ball - in almost all test cases, the ball would either be flung off the platform due to an overly aggressive response or it would roll off the platform due to a fault occurring 
#  during the response. In fact, a fault occurred in each test where the platform responded to the ball.
#
#  @section sec_video Video 
#
#  As my hardware did was not functioning properly, I worked with Jacob on his testing video here: https://vimeo.com/562019631
#
#  The link above shows our several attempts at balancing the ball, but to little success. There was one attempt that got pretty close at balancing the ball, but the ball didn’t quite roll back to the center, so that 
#  attempt can’t quite be considered as a successful run. Additionally, at the time of recording the videos, data collection wasn’t working properly, so the data plots above don’t reflect any of the attempts shown in 
#  the video. From the various trials in the video, it’s quite clear that the success of the response not only relies on the aggressiveness controller gain values, but also the initial conditions. If the ball is placed 
#  farther away from the center of the touch panel, then the response is generally more aggressive, and if it’s closer, the response is somewhat milder, although all responses would end in divergence.
#
#  The reasons why our balancing platform didn’t perform as expected is primarily due to the lack of testing time. If we had a few more days of testing time, we would have been able to tune our controller and resolve 
#  more bugs in the code. Additionally, we might have been able to implement the alpha-beta filter for the touch panel and encoder data, fix the data collection task, and optimize the frequency at which each task ran 
#  in our scheduler. Implementing an Inertial Measurement Unit (IMU) would have also helped by automatically pre-balancing the balancing platform and providing some error checking for encoder measurements. There were 
#  many challenges with the hardware, so it’s not unreasonable to assume that there are still unknown hardware issues contributing to the poor performance. The model we used to develop the controller gains oversimplified 
#  much of the system, which means the gain values are likely of little value much beyond signalling the relative magnitudes between each gain value. During testing, the gain values were varied within 1 order of magnitude
#  of the derived gain values in addition to some variations with the signs of the gains in the torque calculations. Thus, it’s possible that the responses got worse during testing as the gain values deviated from their 
#  original values. Typically, the gain values kept their approximate relative magnitude while their absolute magnitude was changed in the direction that appeared to better the system response.
#
#  @section sec_data Data
#
#  @image html UI_Example.png "Figure 1. Example of Program Running On Putty Terminal"
#
#  Disclaimer: For some reason, the programm will always enter a fault state automatically on start up. This can be easily circumvented by pressing the all clear key to return to the idle state. 
#  Additionally, the data collection task wasn’t writing data to the csv because the system would reach a fault state before the data collection task was able to record data. Thus, to plot the 
#  faulty responses, we printed the recorded data to the Putty REPL after the fault was detected. Then, we copied the data and tabulated responses with 8 or more data points in a spreadsheet, 
#  which is shown below. Timestamps were approximated with a time step equal to the period of the touch panel and encoder tasks because not all the data was collected at the same time. 
#  Furthermore, collecting the timestamps of each data point would be difficult to accurately estimate and it would lead to confusing data presentation.
#
#  @image html Data_Points.png "Figure 2. Data Collection for three tests: X, Y, X Dot, Y Dot, Theta X, Theta Y, Theta X Dot, Theta Y Dot"
#
#  The plots shown below compare the responses of two similar tests with the same number of data points. The first plot compares the path of the ball between the two tests in the plane of the touch
#  panel as time passes. The second and third plots compare how the velocity responses in the X and Y directions vary with time for each test case. For each plot, the blue circles represent data 
#  from test 1 and the orange triangles represent data from test 2.
#
#  For Results from the Touch Panel, please refer to @ref Calibration_and_Tuning 
#
#  @section sec_Plots Plots
#
#  @image html plot_1.png "Figure 3. Ball Position on Touch Panel at Multiple Points in Time"
#
#  In Figure 3, you will find the most meaningful plot we generated. It shows the path the ball takes during the balancing routine. The axes have been adjusted to approximate the dimensions of the 
#  touch panel, and the timestamps for each data point have been added to show the general path the ball took as time went on. The cluster of points at the beginning of each response suggest the ball 
#  rolls around a bit as the system moves slightly to adjust the position of the ball. Then, the response of the system diverges as time passes, with a fault likely occurring around 80 ms for test 1 and 
#  around 160 ms for test 2. The responses both seem to travel in the correct direction along the x axis, but they fall off along the y axis. This suggests an error in either the gain values for the y 
#  response (these were assumed to be the same as the gain values for the x axis), in the measured data for calculating the y axis response, or an error in the calculation of the y axis response.
#
#  @image html plot_2.png "Figure 4. Velocity Along the X Asis vs Time"
#
#  @image html plot_3.png "Figure 5. Velocity Along the Y Asis vs Time"
#
#  The velocities are approximated by using the change in position in between touch panel task runs divided by the period between touch panel task runs. The velocity responses do appear to follow the 
#  expected response given the path of the ball shown in the first plot. That is, they increase slightly in the positive direction and then diverge to the negative direction as the controller attempts 
#  to overcompensate for the increasing deviation from the mean. This response makes the most sense for the Y axis since it’s generally greater in magnitude than the response in the X axis, which occurs 
#  due to the ball falling off in the Y direction. The response in the X direction should be positive, but it’s likely that due to the fault occurring early in the response, the controller was unable 
#  to properly compensate.
#
#  @page Source_Code Term Project: Source Code
# 
#  The code for the entirety of the project can be located here: https://bitbucket.org/alyssacliu/me405_termproject/src/master/
#