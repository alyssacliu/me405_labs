"""@file thinkFast.py 
@brief This file contains a python script to utilize external hardware on the nucleo to build a reaction game.
@details    Using the Nucleo board and the built-in Button and LED, this following program uses an external interrupt 
			to trigger a call back function. This call back function is triggered everytime the user presses the button.
			Similarly, the python script uses the timer to properly clock when instances occur.
@author Alyssa Liu
@date May 2, 2021
"""
import utime
import micropython
import pyb
import random

## Emergency buffer to store errors thrown inside ISR
micropython.alloc_emergency_exception_buf(100)

## Timer Object using timer 2 - with time time unit of 1 microsecond
timer_src = pyb.Timer(2, prescaler= 80, period=0x7FFFFFFF)

## Initalize input button
input_button = pyb.Pin(pyb.Pin.cpu.C13, mode=pyb.Pin.IN)

## Initalize LED button
LED_output = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT)

## Initalize early_input boolean variable to trigger if user inputs before LED turns on
early_input = False

## Initalize button_input boolean variable to trigger when user inputs 
button_input = False

reactTime = 0x7FFFFFFF

## Initalize empty list to hold values
reactRecords = []

## Set time_start variable equal to 0 to begin
time_start = 0

def callback(timer_src):
	""" @brief This function creates an Interrupt Service Routine (ISR) triggered when user presses button.
        @details    Function uses global variables to set reactiontime equal to current time (counter)
        			and checks delay variable to see if user pressed button before LED turned on if so the function
        			sets early_input True. Regardless, the button_input boolean variable is set to True 
		@param timer_src timer object using Timer 2
    """
	global early_input
	global button_input
	global reactTime
	global time_start
	global delay
	
	reactTime = timer_src.counter()

	if(delay > utime.ticks_diff(utime.ticks_us(),time_start)):
		early_input = True

	button_input = True


try:
	while True:
		
		## Ensure that global variables are set properly (at reset position)
		reactTime = 0x7FFFFFFF
		button_input = False
		early_input = False
	
		## Record start time
		time_start = utime.ticks_us()

		print('Get Ready...\n')

		## sets delay to be a random time between 2-3 seconds	
		delay = random.randint(2000000, 3000000)

		## delay for the amount of time delay is set to
		pyb.udelay(delay)

		## LED turn on
		LED_output.on()

		timer_src.counter(0)

		## Delay 1 sec
		pyb.delay(1000)

		LED_output.off()

		## If ISR triggered, button_input set to true.
		if (button_input == True): 
			## If early_input is True, then user pressed button too early & time is not recorded
			if (early_input == True):
				print("Button Pressed too early -- no time recorded \n")
			## Else, the reactTime is appended to list and printed out
			else:
				reactRecords.append(reactTime)
				print('Nice! Reaction Time recorded:{:.2f} ms'.format(float(reactTime/1000))+'\n')

		## If ISR is not triggered, then list appends 1 second.
		else:
			reactRecords.append(1000000)
			print('Yikes! Too slow... Reaction Time recorded:{:.2f} ms'.format(float(reactTime/1000))+'\n')
			pass

except KeyboardInterrupt:
	## Create new list to removed any values that are recorded as 0x7FFFFFFF
	outputRecord = []
	for each in reactRecords:
		if each != 0x7FFFFFFF:
			outputRecord.append(each)
	## As long as list is greater than 0, calculate average and print out
	if len(outputRecord) > 0:
		average_reaction = (sum(outputRecord)/len(outputRecord))
		print('Average reaction time in microseconds: ', str(average_reaction))
		print('Average reaction time in milliseconds: {:3.1f}'.format(average_reaction/10**3))
		print('Average reaction time in seconds: {:1.4f}'.format(average_reaction/10**6))

	else:
		print("No Reactions Recorded")