'''
@file Lab0x02

@page Lab0x02

@section sec_title Lab0x02: Think Fast!

@author Jacob Burghgraef

@date 5/1/2021

@section sec_overview Overview

This lab, titled "Think Fast!", implements a reaction time measurement game on
a Nucleo development board using two different methods. The first method tests
the user's reaction time using the utime module as the reference timing system,
and the second method uses the built-in features of the timer modules in the 
STM32 microcontroller as a reference timing system. Both methods test the 
user's reaction time by waiting a randomly generated time between 2 and 3 
seconds, and then proceeding to blink on for 1 second, or until the USER 
button is pressed. As soon as the button is pressed, a system interrupt occurs
on the hardware, at which point the timestamp is recorded and a reaction time
is calculated. The program will run as many times as the user desires until 
they press ctrl+C, where the program outputs the average reaction time. 
Additionally, if the user fails to press the button, the program terminates
and instructs the user on how to properly play the game.

@section sec_method1 Method 1: External Interrupt and utime

In this method, I employ an external interrupt calling back to an ISR that
records the timestamp (via the utime module) when there's a falling edge on
the Pin connected to the USER button. That is, when the user presses the 
USER button, a falling edge occurs on the digital signal read on Pin C13, 
which generates an Interrupt Service Routine (ISR) that records the time at 
which that falling edge took place in a callback function. To record the 
reaction time, the program generates the randomized wait time, waits that 
amount of time, then it records the start time, and begins the LED blink. When 
the user presses the button, that triggers the external interupt that records 
the 'press' time, and then a reaction time is calculated using the difference 
in ticks between the press time and the start time. For more information 
regarding the documentation of this function, please see Lab0x02_Part_A.py \ref

@section sec_method2 Method 2: OC and IC using the Timer function

This method uses Output Compare (OC) and Input Capture (IC) and 2 different
channels of the pyb Timer function to record the reaction time. To begin, the 
pin connected to the USER button, pin C13, must be physically shorted to 
another pin that allows for IC, which is pin B3 in this case. OC uses the 
Timer hardware to compare the timer count to a specific compare value, which
then trigger an action on Pin A5 - the pin that controls the status of the LED.
In my program, the timer channel using OC is set to trigger an action whenever
a the Pin's status is detected as a high voltage (a 1 or 'True' in binary 
terms). This action toggles the pin, which turns the LED off if the LED is on, 
or on if the LED is off. This action occurs at a regular interval set by the
comparison value, which is modified everytime the timer channel references the
callback function OC_cb. This function coordinates the timing of LED blinks 
based off of various cases; in general, the function first retrieves the 
current comparison value, updates it with the appropriate interval (based off
of the current state of the LED, this can be the wait time or 1 second), 
checks for overflow with the timer period, and then sets the new comparison
value. On the other hand, IC uses the timer hardware to capture timer count
values when an external interrupt occurs on Pin B3. I set the timer channel
using IC to capture the timer count in a callback function whenever a falling 
edge is detected on Pin B3. Physically, this represents the USER button being
pressed because while the button is unpressed, the voltage on the pin is high,
and when the button is pressed, the voltage on the pin is low. The reaction 
time is calculated when an ISR calls back to the IC callback function, where 
the difference between the captured timestamp (from the IC callback function) 
and the most recent output comparison value is multiplied by the prescaler 
divided by the clock speed to get the reaction time in microseconds. That is,
reaction time = PS/(80 Mhz)*(IC_value - Last_OC_Value). Note that the timer in
this method is initialized with timer 2 and appropriate period and prescaler
values such that the base time unit of the clock or 'tick' is 1 microsecond. 

There are two additional notes to using this method. First, an emergency 
memory buffer of 100 bytes should be allocated in case any errors occur during 
the callback. Second, to ensure the LED is turned off at the start of the 
program, the timer channel using OC should be initialized with no callback 
reference and the pin is forced to be inactive. Then, after the emergency 
memory buffer is first allocated, this timer channel can be reconfigured to 
toggle the LED whenever the OC callback function is reference. Additionally, 
the initial comparison value should be set here. For further information 
regarding the documentation of this method, please see Lab0x02_Part_B.py \ref

@section sec_results Results and Discussion

Here is the link to a video demonstrating the extent of the functionality of 
my programs: https://vimeo.com/544136523

I was able to successfully implement full functionality of Part A, but part B 
proved to be more challenging. I got the OC callback to work correctly, but
the input capture callback function doesn't seem to return any values when the
button is pressed. The result is a file that calculates negative reaction 
times since the IC value is never updated. This has two probable causes: the 
first being improper implementation of the code in initializing Pin B3, 
initializing the IC timer channel, or the IC callback function. The other 
cause is malfunctioning hardware, specifically the jumper connecting Pin C13 
to Pin B3 incorrectly transmitting the signal or a failure somewhere on the 
board. I think it's more likely to be the former due to my inexperience with 
coding IC in MicroPython. 

In this application, it's difficult to say if either method is superior 
because the timescale of the human reaction time is on the hundreds of 
milliseconds, so higher accuracy down to tens of microseconds is rather 
negligible. However, if this microcontroller was controlling a motor with a 
high output angular velocity on the scale of thousands of RPMs, higher 
accuracy on the microsecond timescale becomes crucial, especially for high
precision work done with the motors. The more accurate the timestamp/time 
interval of a system, the better signal attenuation can be when controlling 
the motor, producing smoother, faster torque compensation when the load on the
motor changes unexpectedly. Of course, this is somewhat of a generalization of
a singular additional application - there are many other applications that 
could benefit from high accuracy/high resolution timing with MCUs. In these 
applications operating on the microsecond time scale, the IC-OC method is 
superior to the external interrupt method. This is because the IC-OC method
records data as the actions are performed (as the button is pressed and as the
LED turns on) with minimal code in between. On the other hand, the external 
interrupt method records the start time before the LED is turned on, and then 
records the timestamp of the falling edge of the button press signal only 
after extra code in the ExtInt function is executed. Additionally, the time 
module utime and the ExtInt module are uncoupled (not direclty related), 
unlike the built-in IC and OC functionality of timer channels, which means the 
timestamps collected by utime won't directly correspond to the ISRs generated 
by the ExtInt function. Hence, the first method is less accurate on smaller 
time scales where execution time becomes more relevant.
'''