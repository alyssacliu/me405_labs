"""
@file lab3_ui.py
Documentation of lab3_ui.py

@author Alyssa Liu
@date May 9, 2021
"""
import serial
import time
import matplotlib.pyplot as plt

def plotData(x, y):
    """
    @brief      Processes and plots data using the matplot lib library
    @details    Using passed parameters, the function plots the data for the given values using matplotlib 
                and labeling the x and y axis as well as titling output plot
    @param      x values to plot
    @param      y values to plot
    """
    y = [val*3.3/4095 for val in y]

    pyplot.plot(x, y, "r-")
    pyplot.xlabel("Timestamp (microsecond)")
    pyplot.ylabel("Voltage (in Volts)")
    pyplot.title("Voltage vs. Time")
    pyplot.show()

def connection(serial_data, data_package):
    """
    @brief      function displays any connection based actions and information
    @details    Function reads and writes values to display interrupts and ADC data.\
    @param      serial_data is the serial connection with the board
    @param      data_package is the input key value
    """
    serial_data.write(str(data_package).encode('ascii'))
    
    ## Stall
    time.sleep(0.01)                               
    
    ## Pull connection
    return_val = str(serial_data.readline().decode('ascii')).strip()
    
    print("To the board: %s --- From the board: %s" %(str(data_package), return_val))

    if(data_package == return_val):
        return return_val    

def UI_init(key):
    """
    @brief      UI inital start up message
    @details    Function prints welcome message and instructions. Similarly, the function continously requests
                user input until submitted and/or CTLR-C is pressed. After recieving the key the program performs a connection with the board 
    @param      key is the desired key
    """
    input_Key = 'z'

    print("Welcome! Please enter '%s' to begin." %key)

    while input_Key.lower() != key:
        input_Key = input("Enter '%s': " %key)


def main():
    """
    @brief      Main function
    @details    Initalizes variables and calls functions to begin UI script
    """

    key = 'g'

    ser = serial.Serial(port = '/dev/tty.usbmodem205F316230562', baudrate = 115273, timeout = 1)
    times = []
    voltages = []
    begin = False
    data = None

    UI_init(key)

    while(begin is False):    
        begin = (connection(ser, key) == key)

    while(data != 'start'):
        print('waiting for start')
        data = ser.readline().decode('ascii').strip()
        print(data)

    data = ser.readline().decode('ascii').strip()

    while(data != 'end'):
        dps = data.split(',')         
        times.append(int(dps[0]))       
        voltages.append(int(dps[1]))    
        data = ser.readline().decode('ascii').strip()

    with open('lab3Results.csv', 'w') as file:
        for n in range(len(times)):
            line = ("%04d, %.04f\n" %(int(times[n]), float(voltages[n])))
            file.write(line)

    plotData(times, voltages)

    print("voltages:", end = '')
    print(voltages)
    print("times:", end = '')
    print(times)
    
    ser.close()

if __name__ == '__main__':
    main()