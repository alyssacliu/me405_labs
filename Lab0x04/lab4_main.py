"""
@file lab4_main.py
@brief      main file for Lab 0x04
@details    Starts program for nucleo and temperature sensor to connect via I2C digital interface. The main file will run for 
            8 hours (480 minutes) or until key-board interrupt. It will then output a csv file with temperature readings of the
            sensor and board.
@author Alyssa Liu
@date May 13, 2021
"""

import os
import pyb
from utime import sleep
import mcp9808
from MCP9808 import MCP9808

def getInternalTemp(adcall):
    """
    @brief      Reads the internal temp of the board
    @details    Pulls the internal temperature on the board
    """
    temp = adcall.read_core_temp();
    return temp


def main():
    """
    @brief      Main.poy
    @details    On the nucleo, this script starts the I2C connection with temperature sensor,
    reads and stores returned values for a set duration (480 minutes or 8 hours)
    """
    adcall0 = pyb.ADCAll(12, 0x70000)

    filename = 'temperature_data.csv' 
    
    hours_eight_in_min = 480
    count = 0

    i2c_object = pyb.I2C(1, pyb.I2C.MASTER, baudrate = 400000)
    mcp9808_object = MCP9808(i2c_object, 24)

    try:
        while(True):
            if(count < hours_eight_in_min):
                with open(filename, "w") as temp_file:
                    temp_file.write("Time (minute), Temperature Board (degree C), Temperature Sensor (degree C)\r\n")
                    temp_file.write("{:}".format(count))
                    temp = getInternalTemp(adcall0)
                    temp = (temp*0.207)+21.477         
                    temp_file.write(", {:}".format(temp))
                    i2c_call = mcp9808_object.celsius()
                    print(temp)
                    print(i2c_call)
                    temp_file.write(", {:}\r\n".format(i2c_call))
                    sleep(60)
                    count += 1

    except KeyboardInterrupt:
        os.remove(filename)

if __name__ == '__main__':
    main()

