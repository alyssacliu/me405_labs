"""
@file DCMotorsFD.py

@subpage ME405DCM DC Motors

@section sec_titleDCM DC Motors: Fault Detection

@author Jacob Burghgraef and Alyssa Liu

@date June 11th, 2021

@section sec_overview Overview

To protect the motors and prevent mechanical failures in our balancing plate
system, fault detection needed to be implemented in the motor driver and in 
each task. Three distinct requirements were identified for fault detection:
    1. Trigger an external interrupt callback when the nFAULT pin goes low.
    2. As a result of the interrupt callback, all motor functionality must 
    stop until you clear the fault with some form of deliberate user input.
    3. You must be able to clear the fault and resume operation without 
    resetting the microcontroller.
    
@section sec_fault How We Implemented Fault Detection

Fault detection is first detected in the Motor Driver class DRV8847.py, which 
uses an external interrupt to raise a fault detection flag. This flag is then 
read as an attribute of the motor driver object instantiated in the motor task, 
where the nFAULT pin is activated when the flag is raised. As this nFAULT pin 
is activated, the DRV8847 motors go into a fault state where the motors are 
unable to be used until the nFAULT pin is deactivated. Back in the Motor Task 
class, when the nFAULT pin is activated, the motor task will raise a fault 
flag and then share this information with the other tasks using a shared 
variable in the TPShares.py file. \ref

The UserTask.py file runs at a higher frequency than all other tasks, so it 
should detect this fault flag first. Upon detection, the UserTask will lower 
the fault flag, the begin balancing flag and the all clear flag, and raise a 
halt flag using TPShares file. Upon the next iteration of the Encoder, Touch 
Panel, Controller, and Data Collection tasks, the halt flag will be detected, 
which will send these files back into an idle state, where they wait for 
another command from the UserTask before doing anything else. Additionally, 
the UserTask will transition to a fault handling state where instructions on 
how to clear the fault are conveyed to the user. After the user has cleared 
the fault, they will need to enter a command to the UserTask indicating that 
they have cleared the fault.

This next command is the ‘all clear’ command, which has a corresponding flag 
denoted by ‘allclr’; this flag is raised when the user enters an ‘a’ into the 
console on Putty. Upon receiving the all clear from the user, the allclr flag 
is read by the Motor Task and the Controller task, which transition out of 
their fault states. The motor task clears the fault using the clear_fault() 
function in the DRV8847 motor driver, which clears the fault flag in the motor 
class and deactivates the nFAULT pin. The motor task then transitions back to 
its normal run state, but the motors won’t run until the system has been given 
the command to begin balancing again because the duty cycles are set to zero. 
The Encoder, Touch Panel, Controller and Data Collection tasks all immediately 
transition to an idle state where they wait for the ‘begin balancing’ command.

After the fault has been cleared, the system is effectively ‘reset’ to the 
initial idle state just after startup, where the user must pre-balance the 
platform and press ‘b’ to begin balancing the ball. Note that the option to 
force a fault state has been given to the user, where they can press ‘h’ to 
send the controller, encoder, touch panel, and data collection tasks to an 
idle state, and the UserTask will go into a fault detection state. However, 
the motors remain in the normal run state, but the duty cycle is set to zero.

@section sec_file File List
DRV8847.py \ref
TPShares.py \ref

"""