"""
@file hw4.py
@brief This file contains a python script to create a model based on calculations from HW assignment 0x02.
@details    Using the pivoting platform system calculations from HW 0x02, this script calculates matrices based on given
			and inital conditiongs. Similarly, it utlizing sympy and numpy to properly configure the matrices as well as 
			calculate the jacobian values as needed. Note: this script does not plot properly due to complications using 
			matplotlib on python.
@package HW0x04 
@author Alyssa Liu
@date May 9th, 2021
"""
import numpy as np
import sympy as sp
import matplotlib.pyplot as plt
from sympy import Matrix
from sympy import sin as sin
from sympy import cos as cos

#Initalizing 
rm = sp.symbols('rm')
rb = sp.symbols('rb')
rg = sp.symbols('rg')
rp = sp.symbols('rp')
rc = sp.symbols('rc')
lr = sp.symbols('lr')
lp = sp.symbols('lp')
mb = sp.symbols('mb')
mp = sp.symbols('mp')
Ip = sp.symbols('Ip')
Ib = sp.symbols('Ib')
g  = sp.symbols('g')
b  = sp.symbols('b')
Tx = sp.symbols('Tx')
tx = sp.symbols('tx')
ty = sp.symbols('ty')
txd = sp.symbols('txd')
tyd = sp.symbols('tyd')
pos = sp.symbols('pos')
posd = sp.symbols('posd')
w  = sp.symbols('w')
wd = sp.symbols('wd')

## 4x1 Matrix
u = Matrix([Tx])
x = Matrix([posd, tyd, pos, ty])	


## Inital Position 
x0 = Matrix([0, 0, 0, 0])
u0 = Matrix([0])

## Create M Matrix
M11 = -((mb*(rb*rb)) + (mb*(rc*rb)) + Ib)/rb;
M12 = -(Ib*rb + Ip*rb + mb*(rb*rb*rb) + mb*rb*rc*rc + 2*mb*rb*rb*rc + mp*rb*rg*rg + mb*rb*pos*pos)/rb;
M21 = -(mb*rb*rb + Ib)/rb;
M22 = -(mb*rb*rb*rb + mb*rc*rb*rb + Ib*rb)/rb;

M = Matrix([[M11, M12], [M21, M22]])

## Create f Matrix
f1 = b*ty - g*mb * (sin(ty)*(rc+rb)+pos*cos(ty)) + Tx*lp/rm + 2*mb*tyd*pos*posd - g*mp*rg*sin(ty);
f2 = -mb*rb*posd*tyd*tyd - g*mb*rb*sin(ty);

f = Matrix([f1, f2])

## Invert to find accelerations
acc = M.inv()*f

## Calculate Jacobians
Jx = acc.jacobian(x)
Ju = acc.jacobian(u)

## Given Values
provided_values = {
	'rm_val' : 0.060,		# [m]
	'rb_val' : 0.0105,		# [m]
	'rg_val' : 0.042,		# [m]
	'rp_val' : 0.0325,		# [m]
	'rc_val' : 0.050,		# [m]
	'lr_val' : 0.050,		# [m]
	'lp_val' : 0.110,		# [m]
	'mb_val' : 0.030,		# [kg]
	'mp_val' : 0.400,		# [kg]
	'Ip_val' : 0.00188,		# [kg*m^2]
	'Ib_val' : (2/5)*mb*rb*rb,# [kg*m^2]
	'g_val'  : 9.81,			# [m/s^2]
	'b_val'  : 0.010		# [N*m*s/rad]
}
## Calculate A and B Matricies
variables_sub = [rm, rb, rg, rp, rc, lr, lp, mb, mp, Ip, Ib, g, b]
values_sub = list(provided_values.values())

## Substitute variables
if len(variables_sub) == len(values_sub):
	A = Jx
	B = Ju
	for i in range(len(variables_sub)):
		A = A.subs({variables_sub[i]: values_sub[i]})
		B = B.subs({variables_sub[i]: values_sub[i]})

## Substitute inital values
for i in range(len(x)):
		A = A.subs({x[i]: x0[i]})

for i in range(len(u)):
		B = B.subs({u[i]: u0[i]})


## Attempt for Matlab Plotting - Note: pyplot did not allow for the non array variables to be plotted properly
#time  = [x/10 for x in range(100)]
#y = A*x + B*u
#fig, axs = plt.subplots(2, 2)

#y1 = A*x + B*u
#axs[0, 0].plot(time, y)
#axs[0, 0].set_title('Axis [0,0]')
#axs[0, 1].plot(x, y, 'tab:orange')
#axs[0, 1].set_title('Axis [0,1]')
#axs[1, 0].plot(x, -y, 'tab:green')
#axs[1, 0].set_title('Axis [1,0]')
#axs[1, 1].plot(x, -y, 'tab:red')
#axs[1, 1].set_title('Axis [1,1]')
#plt.show()

