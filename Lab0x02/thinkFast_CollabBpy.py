import pyb
import micropython, random
from array import array

# Create pin variables to interact with the Nucleo
## Pin variable representing Pin A5 (the LED) on the Nucleo
pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)

## Pin variable representing Pin PB3 (the User button) on the Nucleo
pinB3 = pyb.Pin(pyb.Pin.cpu.B3)

# Create lists and variables to catalogue reaction times
## A array of the recorded reaction times
rxn_times = array('f')

## A single recorded reaction time, used to add values to the reaction time array
rxn_time = 0

## Average reaction time
avg_rxn = 0

## A counter for the number of times the reaction time is measured
n = 0

## A boolean represent whether or not the User button has been pressed.
button_press = False

## A boolean representing the state of the LED
LED = False

## Duration of the LED blink, in microseconds
LED_dur = 10**6

def IC_cb(Timer):
    '''
    Creates an Input Capture Callback
    '''
    print('IC Callback')
    global IC_value
    global button_press
    IC_value = Timer.channel(2).capture()
    button_press = True

def OC_cb(Timer):
    '''
    Creates an Output Compare Callback
    '''
    print('OC Callback')
    global last_compare
    global button_press
    global wait
    global LED
    
    if (button_press): #If button is pressed
        # LED turns off
        LED = False
        last_compare = T2CH1.compare() # Get the compare value
        
        # Add interval (randomized wait time) to the most recent compare value
        last_compare += wait*1000   
        
        #last_compare &= 0x7FFFFFFF # Check for overflow
        T2CH1.compare(last_compare) # Set the new compare value 
    else:
        if (LED): # If LED is not pressed during light blink duration and it's on
            # LED turns off
            LED = False
            last_compare = T2CH1.compare()
            last_compare += wait*1000   # Add interval to the most recent compare value
            #last_compare &= 0x7FFFFFFF
            T2CH1.compare(last_compare)
        else:
            # The LED is off, so it needs to be turned on
            LED = True
            last_compare = T2CH1.compare()
            last_compare += 10**6   # Add interval to the most recent compare value
            #last_compare &= 0x7FFFFFFF
            T2CH1.compare(last_compare)

# Set up timer and timer channels for the IC/OC 
## The prescalar value for the timer
PS = 79

## The timer object, configured to be a counter 
Tim2 = pyb.Timer(2, period=0x7FFFFFFF, prescaler=PS)

## Timer channel 1, configured for Output compare and connected to the Pin A5
T2CH1 = Tim2.channel(1, pyb.Timer.OC_INACTIVE, pin=pinA5, polarity=Tim2.HIGH, callback = None)

## Timer channel 2, configured for Input capture and connected to Pin B3
T2CH2 = Tim2.channel(2, pyb.Timer.IC, pin=pinB3, polarity=Tim2.FALLING, callback = IC_cb)

# Create variables for IC/OC
## The most recent OC value
last_compare = 0

## The time value captured when the User button is pressed.
IC_value = 0

def genWaitTime():
    '''
    Generates a random wait time between 2 and 3 second, in milliseconds.
    '''
    ## A randomly selected time between 2 and 3 s, in milliseconds.
    wait_time = random.randint(2000, 3000)
    
    print('Generated Wait time:{:1.4f}'.format(wait_time/10**3))
    
    return wait_time

try:
    # For keeping track of unexpected errors
    micropython.alloc_emergency_exception_buf(100)
    
    # Reconfigure timer channel 1 to toggle mode
    T2CH1 = Tim2.channel(1, pyb.Timer.OC_TOGGLE, pin=pinA5, polarity=Tim2.HIGH, callback = OC_cb, compare=(Tim2.counter()+2000000))
    
    T2CH2.callback(IC_cb)
    
    # Generate the initial wait time
    wait = genWaitTime()

    while True:
        # Run Reaction Time Test
        
        pyb.delay(wait+1000)
        
        #if (button_press == False):
            #print('Something went wrong, you did not press the button.')
            #print('Please re-run the program and be sure to press the blue button as soon as the green LED flashes.')
            #break;
        #else:
            #print('Getting new rxn time.')
            # Calculate the reaction time
        rxn_time = (PS/(80000))*((IC_value - last_compare))
            
            # Reset Button Press
        button_press = False
            
            # Reset IC_value
        IC_value = 0
        
        # Append the reaction time value to the reaction time array
        rxn_times.append(rxn_time)
        
        # Increment counter
        n += 1
        
        # Display the reaction time for this run
        print('Reaction ', str(n), ' in microseconds: ', str(rxn_time))
        
        # Generate a new wait time
        wait = genWaitTime()
        
except KeyboardInterrupt:
    Tim2.deinit()
    pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)
    
    print('Ctrl+C detected, displaying results.')
    print('Number of reaction tests run: ', str(n))
    
    # A placeholder for the sum of the reaction times, in microseconds
    rxn_sum = 0
    for i in range(len(rxn_times)):
        rxn_sum += rxn_times[i]       # Calculate the sum of the reaction times
    
    # Calculate the average reaction time, in microseconds
    avg_rxn = rxn_sum/n
    
    print('Average reaction time in microseconds: ', str(avg_rxn))
    print('Average reaction time in milliseconds: {:3.1f}'.format(avg_rxn/10**3))
    print('Average reaction time in seconds: {:1.4f}'.format(avg_rxn/10**6))