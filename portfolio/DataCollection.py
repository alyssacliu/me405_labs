'''
@file       DataCollection.py
@brief      Collects data from the balancing system.
@details    This task collects the data measured by the encoder and touch 
            panel tasks; data is collected for 4 seconds after the 'begin 
            balancing' command is sent by the UI task. After that, the data 
            collected over that four seconds is sent in a batch to a csv. 
            Batched data collection will occur continuously until a halt is 
            detected, upon which the task will then purge collected data and 
            then transition to the idle state. The data measured includes 
            position, velocity, platform angle and platform angular velocity
            data along the x and y axes. The csv can be accessed on the Nucleo.
@author Jacob Burghgraef & Alyssa Liu
@date June 11th, 2021

A state transition diagram for the Finite State Machine (FSM) implemented in 
this task is shown below:
    
@image html "ME 405 Term Project Data Collection FSM.PNG"

The documentation for this file can be found here:
    https://bitbucket.org/alyssacliu/me405_termproject/src/master/DataCollection.py
'''

import utime, TPShares
from array import array

class DataCollection():
    '''
    @brief      Collects data from the balancing system.
    @details    This data collection task collects data from the Encoder task 
                and touch panel task and synchronizes that collected data with 
                a timestamp. As each new data point is recorded, it is written 
                to a csv file. The data recorded includes the position and 
                velocity of the ball in x and y coordinates as well as the 
                angle and angular velocity of the platform in about the x and 
                y axes. Data collection begins automatically as the ball 
                balancing begins and ends automatically as a fault is detected 
                or when the ball is balanced. Alternatively, data collection 
                can be started or stopped at any time by the user. Note that 
                the utime module is used to record timestamp data, the queues 
                marked with a 'dc' from the TPShares file are used to record 
                data, and the array module is used to store data.
    '''
    
    def __init__(self, interval):
        '''
        Creates a data collection task.
        @param interval The interval between data samplings from the closed 
                        feedback loop - this should be the same as the period
                        of the Encoder task and the Touch Panel Task.
        '''
        ## Time interval between Data Samplings, in microseconds
        self.interval = interval*1e6
        
        ## The array of timestamps
        self.t = array('f')
        
        ## The x position data
        self.x = array('f')

        ## The y position data
        self.y = array('f')   
        
        ## The velocity data of the ball in the x direction
        self.xdot = array('f')
        
        ## The velocity data of the ball in the y direction
        self.ydot = array('f')
        
        ## The theta x angular position data
        self.th_x = array('f')

        ## The theta y angular position data
        self.th_y = array('f')   
        
        ## The angular velocity data of the ball in the x direction
        self.th_xdot = array('f')
        
        ## The angular velocity data of the ball in the y direction
        self.th_ydot = array('f')
        
        ## The current timestamp
        self.curr_time = 0
        
        def run():
            '''
            @brief  Runs 1 iteration of the task.
            '''
            if self.state == 0:
                # Initialization state
                self.state = 1
                    
            elif self.state == 1:
                # Idle state                        
                if TPShares.begbal.get() == 1:
                    # Collect data when the platform starts balancing the ball
                    
                    # The start time for data collection; occurs in the 
                    # next loop.
                    self.data_start_time = utime.ticks_us() + self.interval
                    
                    self.state = 2

            elif self.state == 2:
                # Collect data state
                if TPShares.halt.get() == 1:
                    # Clear data arrays
                    del self.t[:]
                    del self.x[:]
                    del self.y[:]
                    del self.xdot[:]
                    del self.ydot[:]
                    del self.th_x[:]
                    del self.th_y[:]
                    del self.th_xdot[:]
                    del self.th_ydot[:]
                    
                    # Stop data collection, go back to idle state
                    self.state = 1
                    
                else:
                    # Get all the recorded data in a batch
                    num_elements = TPShares.xdc.num_in()
                    i = num_elements
                    while i>=0:
                        self.t.append(self.data_start_time + interval*(num_elements-i))
                        i -= 1
                        self.x.append(TPShares.xdc.get()) # Record x position data
                        self.y.append(TPShares.ydc.get()) # Record y position data
                        self.xdot.append(TPShares.xdotdc.get()) # Record x dot velocity data
                        self.ydot.append(TPShares.ydotdc.get()) # Record y dot veloctiy data
                        self.th_x.append(TPShares.th_xdc.get()) # Record theta x position data
                        self.th_y.append(TPShares.th_ydc.get()) # Record theta y position data
                        self.th_xdot.append(TPShares.th_xdotdc.get()) # Record theta x dot velocity data
                        self.th_ydot.append(TPShares.th_ydotdc.get()) # Record theta y dot veloctiy data
                    with open ("BalancingData.csv", "a") as csvfile:
                        # Append the data collected to the csv file
                        csvfile.write("{t}, {x}, {y}, {xdot}, {ydot}, {th_x}, {th_y}, {th_xdot}, {th_ydot}\r\n"
                                      .format (t=self.t[-1],x=self.x[-1],y=self.y[-1],xdot=self.xdot[-1],ydot=self.ydot[-1],
                                               th_x=self.th_x[-1],th_y=self.th_y[-1],th_xdot=self.th_xdot[-1],th_ydot=self.th_ydot[-1]))
            else:
                #Error State
                pass
            
            yield(self.state)