'''
@file ControllerTask.py
@brief Implements closed-loop feedback for the balancing platform system.
@details    This task handles the calculations corresponding with the 
            closed loop feedback controller model of the ball-platform
            balancing system. This is where all the magic happens - the 
            data collected from the encoders and touchpanel is used here
            to calculate duty cycle outputs for the motor. Note: due to 
            hardware and software limitations, the gain values used to 
            attenuate system feedback are precalculated from a previous
            assignment, and are thus static. That is, the gain values 
            don't "react" to the system response and change with each new
            loop. A dynamic controller would need to integrate a complete
            numerical model to calculate new gain values, which would eat 
            up quite a bit of processing power and memory. In a MCU like 
            the Nucleo where resources are rather limited, and given that 
            python runs about as fast as two snails pushing three snails, 
            implementaion of the dynamic controller would hinder performance.
@author Jacob Burghgraef & Alyssa Liu
@date June 11th, 2021

A state transition diagram for the Finite State Machine (FSM) implemented in 
this task is shown below:
    
@image html "ME 405 Term Project Controller FSM.PNG"

The documentation for this file can be found here:
    https://bitbucket.org/alyssacliu/me405_termproject/src/master/ControllerTask.py
'''

import TPShares

class ControllerTask():
    '''
    @brief      A task that controls the closed-loop feedback of the system.
    @details    This task is intended to be used with the balancing platform
                system, for it uses gain values, state variables, and 
                constants like mass, dimensions, and damping coefficients 
                specific to this system. Additionally, the gain (K) values are 
                precalculated using a model derived on paper and implemented 
                in MATLAB. Please see EOMDerivation.py for further details.\ref
                The finite state machine has three states: an idle state that 
                waits for the command 'b' from the UI task before 
                transitioning to the calculation state, where the measured 
                values from the encoders and the touch panel are used to 
                calculate the percent duty cycles for each of the motors. 
                These percent duty cycles are then written to the shared 
                variables file TPShares.py, where they will be retrieved by 
                the motor task. If a fault is detected, then the controller
                task will transition to a third state that waits for an all 
                clear command 'a' from the UI before going back to the idle
                state. Note that the constants initialized at the creation of 
                this object and the output torques are the only callable 
                attributes of this class since all measured values can be 
                accessed wuth the TPShares file.
    '''
    def __init__(self):
        '''
        @brief  Initializes important variables for the ControllerTask object.
        '''
        ## The state of the controller task, represented numerically by an 
        #  unsigned integer.
        self.state = 0
        
        # The controller gain values 
        ## Attenuates position, in units of N
        self.K1 = -5.7941
        ## Attenuates angle, in units of Nm
        self.K2 = -1.5335
        ## Attenuates velocity, in units of N-s/m
        self.K3 = -1.5612
        ## Attenuates angular velocity, in units of Nm-s
        self.K4 = -0.1296
        
        # The motor output torque values calculated by the controller
        self.Tx = 0
        self.Ty = 0
        
        # Motor constants from the DRV8847 Data Sheet, used for converting
        # torque values outputted by the controller to % duty cycles.
        ## Terminal Resistance, in ohms
        self.R = 2.21
        ## Torque constant, in Nm/A
        self.Kt = 13.8/1000
        ## Motor voltage supply, in volts
        self.Vdc = 12
        
    def run(self):
        '''
        @brief  Runs 1 iteration of the controller task.
        '''
        if self.state == 0:
            # Initialization state, transitions to the idle state 
            self.state = 1
            
        if self.state == 1:
            # Idle state, waits for a command to start calculating from the 
            # UItask.
            
            if TPShares.begbal.get() == 1:
                self.state = 2
                
        if self.state == 2:
            # Calculation state, this is where data is read from the encoders 
            # and touch panel via data queues and then a duty cycle to drive 
            # the motor is outputted. Upon each iteration, the halt command is
            # checked before calculation.
            
            if TPShares.halt.get() == 1:
                # UI task wants controller to halt, transition to the idle 
                # state.
                self.state = 1
                
                # Reset the percent duty cycles.
                TPShares.dtyX.put(0)
                TPShares.dtyY.put(0)
                
            else:
                # Let's do some math
                
                # Retrieve the values we need from TPshares
    
                # The following comes from the Touch Panel Task
                x = TPShares.x.get()
                y = TPShares.y.get()
                xdot = TPShares.xdot.get()
                ydot = TPShares.ydot.get()
                
                # The following comes from the Encoder Task
                th_x = TPShares.th_x.get()
                th_y = TPShares.th_y.get()
                th_xdot = TPShares.th_xdot.get()
                th_ydot = TPShares.th_ydot.get()
                
                # Using these measured values, calculate the new Torque output 
                # for each motor using a simplified dot product.
                ## The torque value we need to drive Motor X
                self.Tx = self.K1*x + self.K2*th_y + self.K3*xdot + self.K4*th_ydot
    
                ## Now calculate the torque for Motor Y
                self.Ty = self.K1*y + self.K2*th_x + self.K3*ydot + self.K4*th_xdot
                
                # Now convert the torques to % Duty Cycles
                dtyX = ((100*self.R)/(4*self.Kt*self.Vdc))*self.Tx
                dtyY = ((100*self.R)/(4*self.Kt*self.Vdc))*self.Ty
    
                # Finally, send these off to the Motor Task
                TPShares.dtyX.put(dtyX)
                TPShares.dtyY.put(dtyY)
                
        else:
            # Error State, this should never be reached by the FSM
            pass
        
        yield(self.state)