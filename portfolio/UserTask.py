'''
@file       UserTask.py
@brief      A User interface task.
@details    This is a user interface task that receives commands through putty; 
            commands are simple - the main commands are halt and begin 
            balancing. The overwriting of all inter-task flag variables - with 
            the exception of the fault flag, which is overwritten in the motor 
            task - is handled entirely in this file to prevent conflicts 
            between tasks. Thus, while the scheduler handles the overall 
            timing of the tasks, the UserTask class handles the sequencing of 
            each task within those timing parameters.
@author Jacob Burghgraef & Alyssa Liu
@date June 11th, 2021

A state transition diagram for the Finite State Machine (FSM) implemented in 
this task is shown below:
    
@image html "ME 405 Term Project UserTask FSM.PNG"

The documentation for this file can be found here:
    https://bitbucket.org/alyssacliu/me405_termproject/src/master/UserTask.py
'''

from pyb import USB_VCP
import TPShares

class UserTask:
    '''
    @brief      A task that connects the user to the balancing platform system.
    @details    User interface task; it accepts 3 command inputs from the user:
                    "b" for 'begin balancing', used when the user wants to 
                    start balancing the ball. 
                    "h" for 'halt', used when a fault-flag is received by the 
                    user task; it is reset when the all clear cmd has been 
                    triggered. Normally used as an internal command between 
                    the controller and the user task, it can also be used by 
                    the user to stop the controller.
                    "a" for 'all-clear', used when the fault state has been 
                    rectified by the user.
                There is also an internal command used to handle fault 
                detection:
                    "f" for 'fault flag', used when a fault has been detected 
                    in the motor task.
                Note that commands are read using the USB_VCP class from the 
                pyb module.
    '''
    def __init__(self):
        '''
        @brief Creates a UserTask object.
        '''
        ## The state to run on the next iteration of the task.
        self.state = 0
        
        self.readkey = USB_VCP()
        self.readkey.init(flow = USB_VCP.RTS)
        
    def run(self):
        '''
        @brief Runs one iteration of the UserTask.
        '''
        if (self.state == 0):
            # Initialization state
            self.printcmds()
            self.state = 1
            
        elif (self.state == 1):
            # Wait for a command state
            if TPShares.faultflag.get() == 1:
                # Fault in MotorTask
                self.state = 2   # Transition to state 2
                self.printcmds(cmd = False, instr = False, hlt = True)
                TPShares.halt.put(1) # Tell the controller to stop
                TPShares.faultflag.put(0) # Clear fault flag
                TPShares.begbal.put(0) # Clear begin balancing cmd
                TPShares.allclr.put(0) # Clear the all clear flag
                
            else:
                if self.readkey.any():
                    self.cmd = self.readkey.readline().decode('utf-8')
                    if self.cmd == "b":
                        # Begin Balancing
                        TPShares.begbal.put(1)
                        
                    elif self.cmd == "h":
                        # Halt controller
                        TPShares.halt.put(1) # Raise the halt cmd flag
                        TPShares.allclr.put(0) # Clear the all clear flag
                        TPShares.faultflag.put(0) # Clear fault flag
                        TPShares.begbal.put(0) # Clear begin balancing cmd
                        self.printcmds(cmd = False, instr = False, hlt = True)
                        self.state = 2
                        
                    else:
                        # Invalid command, tell the frontend 
                        print('Invalid Command, please type one of the following commands: ')
                        self.printcmds(instr = False)
                    
        elif (self.state == 2):
            # Fault detection handling state
            
            # Wait for the user to send an 'all-clear' before doing anything
            if self.readkey.any():
                
                self.cmd = self.readkey.readline().decode('utf-8')
                
                if self.cmd == "a":
                    TPShares.allclr.put(1) # Send the all-clear to the Motor Task
                    
                    # Change the halt command flag to False (0) 
                    TPShares.halt.put(0)
                    
                    # Reset the controller and prompt the user to put 
                    print('Fault Cleared, please reset the system and start balancing.')
                    
                    # Go back to command state
                    self.state = 1
            
        else:
            # Invalid state case, error occurs
            pass
    
        yield(self.state)
        
    def printcmds(self, cmd = True, instr = True, hlt = False):
        '''
        Prints the commands available to the user as well as instructions on 
        how the balancing system works.
        @param cmd  A boolean representing whether the commands available to 
                    the user should be printed. Default value is True.
        @param intr A boolean representing whether the instructions on how the  
                    balancing system works should be printed. Default value is 
                    True.
        @param hlt  A boolean representing whether the halt instructions 
                    should be displayed. Default value is False.
        '''
        c = cmd
        i = instr
        h = hlt
        if c:
            print('There are 2 commands available to you, the user: ')
            print('b, for ball balancing; use this when you want to start balancing the ball.')
            print("Note: use the b command after you've prebalanced the platform.")
            print('The system will then finish balancing the platform and calibrate.')
            print('The process is quick enough relative to human reaction time ')
            print('such that you can put the ball on a few moments after you press b.')
            print('h, for halt; use it when you want to put the controller in an idle state.')
            print('Note: this command is generally used during fault detection, and ')
            print('it automatically stops data collection.')
        
        if i:
            print('This balancing platform system balances a ball when you place that ball on the touchpad.')
            print('To begin, pre-level the platform before putting the ball on the touchpad.')
            print("Then, when you're ready, press 'b' and place the ball on the touchpad.")
            print('The platform will then start balancing the ball.')
            print('When the ball has been balanced, you may find the relevant data on a csv on the Nucleo.')
        
        if h:
            print("A fault has been detected in one or more of the Motors.")
            print("Please check the motors for any potential faults and clear them.")
            print("Faults might include motor arms getting caught on wires, or ")
            print("the motor belt becoming misaligned, etc.")
            print("Once you have identified and cleared the fault, please ")
            print("press 'a' to bring the system back to an idle state.")
            print('You will then need to rebalance the platform before balancing the ball.')
