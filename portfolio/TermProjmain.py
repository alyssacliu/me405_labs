""" @file TermProjmain.py
    This file contains a demonstration program that runs some tasks, an
    inter-task shared variable, and some queues. 

    @author JR Ridgely, modified by Jacob Burghgraef and Alyssa Liu
    @date June 11th, 2021

    @copyright (c) 2015-2020 by JR Ridgely and released under the Lesser GNU
        Public License, Version 3. 
        
The documentation for this file can be found here:
    https://bitbucket.org/alyssacliu/me405_termproject/src/master/TermProjmain.py
"""

import pyb
from micropython import alloc_emergency_exception_buf
import gc

from UserTask import UserTask
from MotorTask import MotorTask
from DataCollection import DataCollection
from ControllerTask import ControllerTask
from TPTask import TPTask
from EncoderTask import EncoderTask

import cotask
import task_share
import print_task


# Allocate memory so that exceptions raised in interrupt service routines can
# generate useful diagnostic printouts
alloc_emergency_exception_buf (100)

# =============================================================================

if __name__ == "__main__":

    print ('\033[2JTesting scheduler in cotask.py\n')


    # Create the tasks. If trace is enabled for any task, memory will be
    # allocated for state transition tracing, and the application will run out
    # of memory after a while and quit. Therefore, use tracing only for 
    # debugging and set trace to False when it's not needed


    # Run the UI task at a low priority very quickly ~2 to 5 ms
    UITask = UserTask()
    cotask.task_list.append(cotask.Task(UITask.run, name = 'User_Task',
                                        priority = 0, period = 5, profile = True))
    
    # Run the touch panel, encoder, controller and motor tasks at the same 
    # time interval
    TPTask = TPTask()
    cotask.task_list.append(cotask.Task(TPTask.run, name = 'Touch_Panel_Task',
                                        priority = 1, period = 40, profile = True))
    EncTask = EncoderTask()
    cotask.task_list.append(cotask.Task(EncTask.run, name = 'Encoder_Task',
                                        priority = 1, period = 40, profile = True))
    CtrlTask = ControllerTask()
    cotask.task_list.append(cotask.Task(CtrlTask.run, name = 'Controller_Task',
                                        priority = 1, period = 40, profile = True))
    MotTask = MotorTask()
    cotask.task_list.append(cotask.Task(MotTask.run, name = 'Motor_Task',
                                        priority = 1, period = 40, profile = True))
    
    # Run data collection task every 3 to 4 seconds and write batched data to 
    # the csv all at once. The ball should balance in 3 to 4 seconds, so this 
    # shouldn't be a problem. Running in real time would be ideal since we 
    # can get many more data points, but the timing becomes significantly more
    # complex.
    DCTask = DataCollection(40)
    cotask.task_list.append(cotask.Task(DCTask.run, name = 'Data_Collection_Task',
                                        priority = 0, period = 4000, profile = True))

    # Run the memory garbage collector to ensure memory is as defragmented as
    # possible before the real-time scheduler is started
    gc.collect ()

    # Run the scheduler with the chosen scheduling algorithm. Quit if any 
    # character is sent through the serial port
    vcp = pyb.USB_VCP ()
    while not vcp.any ():
        cotask.task_list.pri_sched ()

    # Empty the comm port buffer of the character(s) just pressed
    vcp.read ()

    # Print a table of task data and a table of shared information data
    print ('\n' + str (cotask.task_list) + '\n')
    print (task_share.show_all ())
    print ('\r\n')

