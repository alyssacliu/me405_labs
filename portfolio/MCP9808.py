"""
@file MCP9808.py
@brief      MCP9808 module class object for lab 0x04
@details    The file contains the MCP9808 class which can be used to initalize an object, read the temperature in celsius and farhenheit,
            and check the location of the I2C device on the nucleo.
@author Alyssa Liu
@date May 13, 2021
"""
import pyb           
from pyb import I2C  


class MCP9808:       
    """
    @brief      MCP9808 class - temperature Sensor
    @details    this class reads from the MCP9808 sensor and inputs a premade object. It uses the 
                i2c_loc of the I2C device to read from sensor and check for address, find temperature in celsius, 
                find temperature in fahrenheit. 
    """
    def __init__(self, I2C_object, i2c_loc):
        """ 
        Initialization Function Stores the parameters input by the user when creating 
        the class object so that they may be used later by other functions.
        @param I2C_object the name of an I2C object already created by the user
        @param i2c_loc the location of the I2C device on the nucleo
        """
        self.I2C_object = I2C_object      
        self.i2c_loc = i2c_loc              
                                            
    def check(self):                    
        """ 
        @brief      I2C Check
        @details    Checks the location of the I2C device on the nucleo
        @return     Address/location of the I2C device
        """
        return self.I2C_object.scan()
    
    def celsius(self):                 
        """ 
        @brief      Measures temperature in Celcius
        @details    This function measures the temperature in celsius by reading 
                    the data from the MCP9808 in the form of two bytes. 
                    Using the sign information from the the bit five of the first bit,
                    following it gets rid of the four MSBs in that bit.
                    
                    Next it bit shifts to the left four times on that same byte, bit shifts
                    four times to the right on the second byte, and then combines them. 
                    Finally, the function seeks to verify if the sign bit was one. 
                    If so, then it subratracts the value from 256. Else, it
                    does nothing and returns that value which will be our temperature.
        @return     temperature - temperature in Celsius
        """
        byte_array_buff = bytearray([0, 0]) 
        self.I2C_object.mem_read(byte_array_buff, self.i2c_loc, 5) 
        flag = 0        
        if (0b00010000 & byte_array_buff[0]):  
            flag = 1

        byte_array_buff[0] = byte_array_buff[0] & 0b00001111

        default_set = 16  
        byte_array_buff[0] = byte_array_buff[0]*default_set 
        
        if (flag == 1):  
            temperature = -(256 - (byte_array_buff[0] +byte_array_buff[1]/default_set)) 
        else: 
            temperature = byte_array_buff[0]+byte_array_buff[1]/default_set 
        
        return temperature
        
    def fahrenheit(self):   
        """ 
        @brief      Measures temperature in Farenheit
        @details    This function measures the temperature in farenheit by calling the celsius function which
                    returns the temperature in celsius, then performs simple conversion to get the temperature in 
                    fahrenheit. 
        @return     temperature - temperature in Fahrenheit
        """
        temperature = (self.celsius()*9/5)+32 #run through celsius value then convert
        return temperature