'''
@file EOMDerivation.py

@subpage ME405EOM EOMDerivation

@section sec_titleEOM Equations of Motion Derivation and Term Project Modelling

@author Jacob Burghgraef and Alyssa Liu

@date June 11th, 2021

@section sec_overview Overview

The equations of motion and modelling for the balancing platform system were 
completed over the first 7 weeks of the class; the relevant assignments are 
HW0x02, HW0x04, and HW0x05. The ultimate goal of this modelling is to get gain 
values for a closed loop feedback controller to use for the balancing platform. 
Each assignment covers a different aspect of getting these gain values, and 
they build off of the work of the previous assignment.

@section sec_HW0x02 HW0x02: Hand Calculations

This assignment covers the hand calculations performed to analyze the 
balancing platform system. Kinetics and kinematics were used to analyze the 
forces and motion of the system, ultimately yielding 2 equations of motion to 
describe the system. Please see @ref hw0x02 for more details.

@section sec_HW0x04 HW0x04: Preliminary Controller Modelling

This assignment covers the implementation of the equations of motion from 
HW0x02 into a Simulink model and MATLAB live script to model the system. This 
model was then used to predict the responses of 2 Open Loop controllers and 
the response of a Closed Loop controller with randomly selected gain values. 
Please see @ref hw0x04 for more details.

@section sec_HW0x05 HW0x05: Gain Value Derivation and Refinement

This assignment builds off the numerical model created in HW0x04 to derive the 
closed loop controller gain values. Once the gain values had been derived, the 
model was refined several times to get gain values that produced a desirable 
response. Please see @ref hw0x05 for more details.

@section sec_file File List

- @ref hw0x02
- @ref hw0x04
- @ref hw0x05
'''