'''
@file       MotorTask.py
@brief      A task that controls 2 motors and handles fault detection.
@details    This task works cooperatively with the other tasks in the 
            scheduler, and runs after an output duty cycle has been computed 
            by the controller. This class initializes the two motors in the 
            balancing platform system using the DRV8847 class and stops the 
            motors when a fault is detected, and waits for an all-clear from 
            the UI Task before clearing the fault. The Duty cycle values 
            received from the controller are shared through TPShares.py
@author Jacob Burghgraef & Alyssa Liu
@date June 11th, 2021

A state transition diagram for the Finite State Machine (FSM) implemented in 
this task is shown below:
    
@image html "ME 405 Term Project Motor Task FSM.PNG"

The documentation for this file can be found here:
    https://bitbucket.org/alyssacliu/me405_termproject/src/master/MotorTask.py
'''

import DRV8847, TPShares
from pyb import Pin, Timer

class MotorTask:
    '''
    @brief      Handles the 2 motors and fault detection in the balancing system.
    @details    This is meant to be used in tandem with the other tasks 
                implemented in the balancing plate system. However, unlike the
                other tasks, this task has no idle state, for the duty cycles
                are always set to zero at initialization and upon reaching a 
                fault state. Thus, there is only a need for two primary states 
                - a 'normal' state that drives the motors and a fault state. 
                The normal state will read whatever duty cycle has been 
                computed by the controller and set the motors at this duty 
                cycle. The normal state always checks for faults in the motor,
                and if it detects any faults, it will raise the fault flag and
                transition to the fault state. There, it waits for the 
                all-clear from the UI task before returning to the normal 
                state.
    '''
    def __init__(self):
         '''
         @brief Creates the motor task object.
         '''         
         ## The duty cycle percent value for the X motor
         self.DTY_X = 0
         
         ## The duty cycle percent value for the Y motor
         self.DTY_Y = 0
         
         ## The state of the FSM running in the Motor Task
         self.state = 0
         
         ## The 'all-clear' from the UI task indicating the event that 
         #  triggered the fault state has been dealt with by the user.
         self.allclr = None
    
    def initMotors(self):
        '''
        @brief      Initializes the DRV8847 motors.
        @details    This function is solely made to keep code neat and 
                    compartmentalized. It could just as easily be placed in 
                    the __init__ function at the beginning of the class.
        '''
        # Create the nSLEEP and nFAULT pins
        self.nSLEEP_pin = Pin(Pin.cpu.A15, Pin.OUT_PP)
        self.nFAULT_pin = Pin(Pin.cpu.B2)

        # Create the Motor Driver Object the will control both motors
        self.Mot = DRV8847(self.nSLEEP_pin, self.nFAULT_pin)

        # The timer to be used for controlling the Motors via PWM
        self.tim = Timer(3, freq = 20000);

        # Create an object representing Motor 1

        # Input Pins
        self.IN1_pin = Pin(Pin.cpu.B4)  # Corresponds to OUT1/M1-
        self.IN2_pin = Pin(Pin.cpu.B5)  # Corresponds to OUT2/M1+
        
        # The motor channel that controls motor 1
        self.MotX = self.Mot.channel(self.IN1_pin, self.IN2_pin, self.tim, 1, 2)    
        
        # Create an object representing Motor 2
        
        # Input Pins
        self.IN3_pin = Pin(Pin.cpu.B0)  # Corresponds to OUT3/M2-
        self.IN4_pin = Pin(Pin.cpu.B1)  # Corresponds to OUT4/M2+
        
        # The motor channel that controls motor 2
        self.MotY = self.Mot.channel(self.IN3_pin, self.IN4_pin, self.tim, 3, 4)
         
        # Allow the motors to be driven
        self.Mot.enable()
        
        # Initialize the shared duty cycle variables at zero
        TPShares.dtyX.put(0)
        TPShares.dtyY.put(0)
    
    def run(self):
        '''
        @brief Runs a new iteration of the Motor Task.
        '''
        if self.state == 0:
            # Initialization State, initializes the motors and transitions 
            # to state 1
                
            self.initMotors()
            self.state = 1
                
        elif self.state == 1:
            # Normal run state, waits for commands from the controller and 
            # then drives the motor accordingly.
                
            if self.Mot.fault():
                # Motor is in fault state, send a character to the UItask 
                # to indicate the fault state, set fault pin high, then
                # transition to fault state in the task.
                TPShares.faultflag.put(1)
                
                self.Mot.nFAULT.high()
                
                self.state = 2
            else:
                # Get X duty cycle value from shares or queue object
                self.DTY_X = TPShares.dtyX.get()
                self.DTY_Y = TPShares.dtyY.get()
                    
                # Drive the motors using the new duty cycle values
                    
                # First the x motor
                self.MotX.setlevel(self.DTY_X)
                    
                # Now the y motor
                self.MotY.setlevel(self.DTY_Y)
                    
        elif self.state == 2:
            # Fault state, wait for a response from the UI task before
            # re-enabling the motors.
            
            self.allclr = TPShares.allclr.get()
                
            if self.allclr == 1:
                self.Mot.clear_fault()
                    
                # Fault has been cleared, return to normal state
                self.state = 1
                    
        else:
            # Error state, this should not be accessed by the task
            pass
            
        yield(self.state)