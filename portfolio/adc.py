"""
@file adc.py
@brief 		main file for Lab 0x03 
@details 	When user presses button on the Nucleo, which is defaulted to high. The button drops from 
			high to low and therefore, the voltage drops until the button is released. In this file, a 
			class uses the data collected from the button being pressed to send data to host computer over
			a UART serial connection with outputs a CSV file. 
@author Alyssa Liu
@date May 09, 2021
"""
from array import array as arr
import pyb
from pyb import Timer, Pin, USB_VCP, ADC, ExtInt
import micropython

class ADC_object:
	"""
	@brief 		ADC Object
	@details	This class performs the primary functions that the lab requires: it check for the connection between computer and
				nucleo, it reads and writes to and from the computer to the nucleo over UART serial connection, and it checks if button is being 
				pressed by user.
	"""
	def __init__(self, pin, btn, led, timer, bus, buff):
		"""
		@param timer - the timer is used to read off ADC values
		@param bus - bus is the UART data bus to read and write data
		@param buff - a variable to use as a buffer to store data to write out
		@param pin - pin for ADC read (input)
		@param btn - button response to be read by ADC
		@param led - LED output
		"""
		self.ADC = ADC(pin)
		ExtInt(btn, ExtInt.IRQ_FALLING, Pin.PULL_NONE, self.button_pressed)
		self.LED = Pin(led, mode=pyb.Pin.OUT_PP)
		
		self.timer = timer
		self.bus = bus
		self.buff = arr('H', (0 for index in range(buff)))

		## checks (1 to 0) value only
		## button_pressed_check - checks to see if button is pressed of not
		self.button_pressed_check = 0	
		
		## connection_check - checks to see if connection valid
		self.connection_check = 0		
		self.LED.off()

	def connection(self):
		"""
		@brief		Connection with host computer
		@details	Stalls until incoming value on UART and returns value. Checks if signal comes in, which triggers conditional which reads and
					stores value. After connect is completed, the interrupt can be triggered. 
		@return uart_val is the value recieved over UART from computer
		"""
		uart_val = None
		while uart_val is None:
			if(self.bus.any()):
				uart_val = self.bus.read()
				self.connection_check = 1	
				self.bus.write(uart_val)

		return uart_val

	def read_ADC(self):
		"""
		@brief 		Read button press voltage
		@details	Read ADC values at rate specified by timer and save to buffer
		"""
		self.ADC.read_timed(self.buff, self.timer)

	def write_ADC(self):
		"""
		@brief		Export data to computer over UART
		@details	Convert ADC values in buffer to voltages and write out to comm port.
		"""
		self.button_pressed_check = 0
		self.bus.write('start\n'.encode('ascii'))
		for n in range(len(self.buff)):
			if(self.buff[n] < 4095 and self.buff[n] > 10):
				time = (n * 50)						# Time in us
				self.bus.write('{:}, {:}\r\n'.format(time, self.buff[n]))
			
		self.bus.write('end\n'.encode('ascii'))
		self.read_ADC()

	def button_pressed(self, address):
		"""
		@brief		ISR - Button pressed by user
		@details	The ISR function occurs when the button is pressed. The function then sets button_pressed_check equal to 1 and turns the LED off.
		@param 		address required ISR return value
		"""
		if (self.connection_check == 1):
			self.LED.off()				
			self.button_pressed_check = 1		


def ADC_Loop(adc_object):
	"""
	@brief		ADC_Loop is main loop that runs through object to read and write to ADC
	@details	Runs main loop on nucleo by checking for connection and turning the LED on. After, if the button is not pressed then the 
				program is stuck in a while loop until the button is pressed.
	
	@param adc_object is an ADC_object that is passed to perform primary functions/methods found in object class.
	"""
	while(True):
		adc_object.connection()
		adc_object.LED.on()						
		
		while(not adc_object.button_pressed_check):
			pass
		
		adc_object.read_ADC()
		adc_object.write_ADC()


def main():
	"""
	@brief		Main function
	@details	The main function is what triggers the program to begin by initalizing variables and 
				calling ADC_loop
	"""
	
	## Allocate memory for ISR debug messages
	micropython.alloc_emergency_exception_buf(100)
	
	## Initalizing Pin button, timer, led, buffer size, and bus variables
	pin0 = Pin.board.PA0    
	btn13 = Pin.board.PC13
	timer_5 = Timer(5, freq=100000)
	led5 = Pin.board.PA5
	buffSize = 500
	uart = USB_VCP()

	## Create ADC_Object
	adcObj = ADC_object(pin0, btn13, led5, timer_5, uart, buffSize)

	## Run Main loop
	ADC_Loop(adcObj)


if __name__ == '__main__':
	main()
