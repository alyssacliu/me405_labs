"""@file vendingMachine.py 
@brief This file contains a python script to operate a vending machine based on HW 0x00 FSM.
@details    Based off of a finite state machine design, the following functions simulate a virtual vending machine. 
            The machine takes in user inputs to simulate: taking in money (1 cent, 5 cents, 10 cents, 25 cents, $1, $5, $10, $20),
            dispense 4 different drink options, and return change when demanded. Through each action, the vending machine will
            output relevant strings onto the virtual LCD.
@author Alyssa Liu
@date April 12, 2021
"""
import keyboard

from time import sleep

input_key = ''

def kb_cb(key):
    """ @brief Callback function which is called when a key has been pressed.
    """
    global input_key
    input_key = key.name

keyboard.on_press(kb_cb)

def VendotronTask():
    """ @brief This function embodies one iteration of the task
        @details    In this function, the script will run through the different states as the user inputs values. The balance list is 
                    initalized, as well as the state variable and state_drink list. The states are: INIT, Hold for Input, Output Drink, 
                    Output Change. 

    """
    ## Initalize global variable in function
    ## User key input
    global input_key

    ## balance - list to indicate money inputted into machine
    ## [pennies, nickels, dimes, quarters, 1 dollar, 5 dollar, 10 dollar, 20 dollar]
    balance = [0, 0, 0, 0, 0, 0, 0, 0]

    ## initalize state variable 
    ## set equal to 0 for init state
    state = 0

    ## initalize list for state and drink selection
    ## used in state 1 and 2 to ensure machine can tell which drink was selected between functions
    state_drink = [1, 0]
    
    while True:
        
        ## 0) INIT - output Welcome Message and move to state 1
        if state == 0:
            print("\nWelcome! Please insert money or select beverage. \n")
            state = 1       # on the next iteration, the FSM will run state 1
        
        ## 1) Hold for Input - cycle through inputs until state change is returned, users can insert money, eject change, and select drinks    
        elif state == 1:
            state_drink = [1, 0]
            while state_drink[0] == 1:
                state_drink = checkInput(balance) 
            state = state_drink[0]
        
        ## 2) Output Drink - User selected drink will be dispensed and new balance calculated
        elif state == 2:
            balance = outputDrink(balance, state_drink[1])
            
            if balance == None:
                print("Thank you!\n")
                balance = [0, 0, 0, 0, 0, 0, 0, 0]
                state = 0   # s2 -> s3
            else:
                print("Please insert money or select beverage. \n")
                state = 1
        
        ## 3) Output Change - return any remaing balance.     
        elif state == 3:
            balance = [0, 0, 0, 0, 0, 0, 0, 0]
            print("\n\nThank you! Please come again!")
            print("\n ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            state = 0   
        
        yield(state)



def checkInput(balance):
    """ @brief This function identified user key input and preforms respective action.
        @details This function accepts user key inputs and will update current balance, identify
                    insufficient funds for purchase, and verify ejecting and/or dispensing selected drink. 
                    The following key inputs are acceptable: 0, 1, 2, 3, 4, 5, 6, 7, c, C, s, S, p, P, d, D, e, and E.
                    Any other input will result in error message. 
        @param balance A list to represent user inputted money into machine
                    Ex: balance = [pennies, nickels, dimes, quarters, 1 dollar, 5 dollar, 10 dollar, 20 dollar]
        @return Inputs 0-7 will update current balance with the balance index value respectively. 
                    Inputs e and E will move machine to state 3 and eject remaing balance. 
                    The remain acceptable inputs will compare balance with selected drink's price and either
                    print "Insufficent Funds Error" or move machine to state 2 by returning two index list: [state, drink selection] 
                    to dispense drink and recalculate balance.           
    """   
    ## Note: Identified drink prices as the following ~ Cuke = $1.00, Popsi = $1.20, Spryte = $0.85, Dr. Pupper = $1.10
    global input_key

    while True:
        while input_key:
            if input_key == '0':
                balance[0] += 1
                print("\n\nCurrent Balance: {:.2f}".format(float(toCents(balance)/100))+'\n')
                input_key = None
                break
            elif input_key == '1':
                balance[1] += 1
                print("\n\nCurrent Balance: {:.2f}".format(float(toCents(balance)/100))+'\n')
                input_key = None
                break
            elif input_key == '2':
                balance[2] += 1
                print("\n\nCurrent Balance: {:.2f}".format(float(toCents(balance)/100))+'\n')
                input_key = None
                break    
            elif input_key == '3':
                balance[3] += 1
                print("\n\nCurrent Balance: {:.2f}".format(float(toCents(balance)/100))+'\n')
                input_key = None
                break
            elif input_key == '4':
                balance[4] += 1
                print("\n\nCurrent Balance: {:.2f}".format(float(toCents(balance)/100))+'\n')
                input_key = None
                break
            elif input_key == '5':
                balance[5] += 1
                print("\n\nCurrent Balance: {:.2f}".format(float(toCents(balance)/100))+'\n')
                input_key = None
                break
            elif input_key == '6':
                balance[6] += 1
                print("\n\nCurrent Balance: {:.2f}".format(float(toCents(balance)/100))+'\n')
                input_key = None
                break
            elif input_key == '7':
                balance[7] += 1
                print("\n\nCurrent Balance: {:.2f}".format(float(toCents(balance)/100))+'\n')
                input_key = None
                break
            elif input_key == 'e' or input_key == 'E':
                print("\n\nReturning Change: {:.2f}".format(float(toCents(balance)/100))+'\n')
                input_key = None
                return [3, 0]
            elif input_key == 'c' or input_key == 'C':
                if balance == None or (toCents(balance)) < 125:
                    print("\n\nInsufficient Funds")
                    print("Price for Cuke is $1.00 ~ Current Balance is: {:.2f}".format(float(toCents(balance)/100))+'\n')
                    input_key = None
                    break
                input_key = None
                return [2, 0]
            elif input_key == 'p' or input_key == 'P':
                if balance == None or (toCents(balance)) < 100:
                    print("\n\nInsufficient Funds")
                    print("Price for Popsi is $1.20 ~ Current Balance is: {:.2f}".format(float(toCents(balance)/100))+'\n')
                    input_key = None
                    break
                input_key = None
                return [2, 1]
            elif input_key == 's' or input_key == 'S':
                if balance == None or (toCents(balance)) < 150:
                    print("\n\nInsufficient Funds")
                    print("Price for Spryte is $0.85 ~ Current Balance is: {:.2f}".format(float(toCents(balance)/100))+'\n')
                    input_key = None
                    break
                input_key = None
                return [2, 2]
            elif input_key == 'd' or input_key == 'D':
                if balance == None or (toCents(balance)) < 110:
                    print("\n\nInsufficient Funds")
                    print("Price for Dr. Pupper is $1.10 ~ Current Balance is: {:.2f}".format(float(toCents(balance)/100))+'\n')
                    input_key = None
                    break
                input_key = None
                return [2, 3]
            else:
                print("\n\nInvalid Input --- Try again")
                input_key = None
                break


def getChange(price, payment):
    """ @brief This function calculates change based on current balance and price of drink then outputs change.
        @details The function calls toCents and update_payments to calculate the amount of change the user will 
                    recieve after purchase and puts it back into the list format. If the amount of change to be returned in 0, 
                    then the function will return None. 
        @param payment A list to represent user inputted money into machine
        @param price An integer of price in pennies (i.e $1.25 = 125)
        @return The updated payment list or None
    """ 
    paymentInCents = toCents(payment)

    change = paymentInCents - price

    if (change <= 0):
        return None;

    else:
        payment = update_payment(change)
        return payment


def toCents(payment):
    """ @brief This function converts user inputted balance into cents.
        @details The function traverse through payment parameter and adds to payment_Total integer depending on index value in list.
                    Ex: Index 0 = pennies, thus if payment[0] = 4 then 4 is added to the payment_total integer.
        @param payment A list to represent user inputted money into machine
                    Ex: payment = [pennies, nickels, dimes, quarters, 1 dollar, 5 dollar, 10 dollar, 20 dollar]
        @return An integer with payment list values translated into cents
    """ 
    
    ## initalize payment_Total and set equal to 0
    payment_Total = 0
    
    if (payment[0] != 0):
        payment_Total += payment[0]

    if (payment[1] != 0):
        payment_Total += payment[1]*5
    
    if (payment[2] != 0):
        payment_Total += payment[2]*10

    if (payment[3] != 0):
        payment_Total += payment[3]*25

    if (payment[4] != 0):
        payment_Total += payment[4]*100

    if (payment[5] != 0):
        payment_Total += payment[5]*500

    if (payment[6] != 0):
        payment_Total += payment[6]*1000

    if (payment[7] != 0):
        payment_Total += payment[7]*2000
    
    return payment_Total

def update_payment(change):
    """ @brief This function creates a new balance/payment list with the updated values after purchase. 
        @details In this function, the change is divided by each of the index values of paymentSetUp. If the result is greater than 1,
                    then the updatedPayment list is updated with the ones position of the result. (i.e 2.5 => updatedPayment[x] = 2). 
                    The amount updated is subtracted and the index of updatedPayment is moved towards the front. 
        @param change An integer representing the amount the user is owed after purchase in cents (i.e $1.25 = 125)
        @return A list with updated balance
    """ 
    ## Initalize list with cent values of [$20, $10, $5, $1, 25 cents, 10 cents, 5 cents, and 1 cent]
    paymentSetUp = [2000, 1000, 500, 100, 25, 10, 5, 1]
    
    ## Initalize varible and set equal to 7 to equal lenght of updatedPayment list
    index = 7

    ## Initalize list with for new balance
    updatedPayment = [0, 0, 0, 0, 0, 0, 0, 0]

    ## Traverse each value in paymentSetUp
    ## Update updatePayment starting with the 7th index
    ## Move forward on updatePayment list and backwards on paymentSetUp
    for each in paymentSetUp:
        valid = change/each
        if (valid >= 1):
            updatedPayment[index] = int(valid)
            change -= int(valid)*each
        index -= 1

    return updatedPayment

def outputDrink(payment, drink):
    """ @brief This function constructs print statement and calculates new balance.
        @details In this function, the drink price and drink name is chosen through drink parameter. The dispense statement is printed 
                    and the price of drink is used to calculate change by calling the getChange() function. 
        @param payment A list to represent user inputted money into machine
                    Ex: payment = [pennies, nickels, dimes, quarters, 1 dollar, 5 dollar, 10 dollar, 20 dollar]
        @param drink An integer that identifies which drink was selected via user input
                    Ex: 0 = Cuke, 1 = Popsi, 2 = Spryte, and 3 = Dr. Pupper
        @return The updated balance list based on user selected drink and inputted balance
    """ 
    ## Initialize lists that indicate drink prices and titles that can be indexed via drink parameter
    ## Note: Identified drink prices as the following ~ Cuke = $1.00, Popsi = $1.20, Spryte = $0.85, Dr. Pupper = $1.10
    drinkPrices = [100, 120, 85, 110]
    drinks = ["Cuke", "Popsi", "Spryte", "Dr. Pupper"]

    price = drinkPrices[drink]
    soda = drinks[drink]

    print("\n{0} has been dispensed.\n".format(soda))
    
    return getChange(price, payment)


if __name__ == "__main__":

    ## Calling VendotronTask
    vendo = VendotronTask()
    
    ## Run the task every 10ms (approximately) unless we get a ctrl-c or the task stops yielding.
    
    try:
        while True:
            next(vendo)
            sleep(0.01)
            
    except KeyboardInterrupt:
        keyboard.unhook_all()
        print('Ctrl-c detected. Goodbye')
            
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')
