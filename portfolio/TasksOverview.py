"""
@file TasksOverview.py

@subpage ME405TOV Tasks

@section sec_titleTOV A High Level Overview of the Tasks

@author Jacob Burghgraef and Alyssa Liu

@date June 11th, 2021

@section sec_overview Overview

Our task structure is designed in a way that allows us to seamlessly share 
variables throughout the program to ensure they are updated and adjusted as 
data is collected and outputs are sent to the motors.

@section sec_HLTD High Level Task Diagram

Shown below is the schematic showing how each task communicates with other 
tasks in addition to what is communicated, task priority, and the period at 
which the task is run.

@image html high_level_task.png "Figure 1. High Level Task Overview"

@section sec_how How Does It Work?

When the user starts the program, any input will be through Putty to the 
Usertask. As the each task in the system recieves the command to begin 
balancing the ball, the controller task will reads from the  Touch Panel Task 
and the Encoder Task, to calculate the duty cycles for the Motor Task. 
Simultaneously, all data is recorded recorded in data queues from the Encoder 
Task and the Touch Panel Task. After 4 seconds of data collection, the batch 
of data will be sent to the Data Collection Task, which then writes that data 
to a CSV file on the Nucleo. Note that the flags and values between all tasks 
on the left (all tasks not including the data collection tasks) are shares 
while the data sent to the data collection task are queues. These 'internal' 
shares between the tasks are used because the flags and values need to be read 
multiple times, so a queue wouldn't work. Moreover, previous data points don't
matter in most cases, so a sequence of data for calculations is unnecessary. 
Shares also use less memory, so queues should be used sparingly since memory 
issues are already prevalent. The Data Collection Task requires a sequence of 
data points to properly record batched data, so a queue makes sense here. If 
the data collection were in real time, it would be more accurate and less 
memory intensive, but the timing would be significantly more complicated. This 
is why data collection is batched.

@section sec_file File List

- @ref UserTask.py
- @ref ControllerTask.py
- @ref MotorTask.py
- @ref EncoderTask.py
- @ref TPTask.py
- @ref DataCollection.py
"""