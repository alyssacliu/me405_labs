'''
@file TouchPanelTest.py
This class file is a driver for a resistive touch panel. The touch panel is 
meant to be rectanglular in shape and relatively small, but it could be 
modified to accept other simple geometric shapes for the touch pad. The 
functions contained within this driver include an x scan and a y scan that 
output the position of a single object in contact with the surface of the 
touch panel, a z scan that outputs a boolean signalling whether or not 
something is in contact with the touch panel, and an optimized combined scan
function that runs the x, y, and z scan as quickly as possible. Note: This 
file works best when you pass in the center as [0,0].

Documentation can be found here:
    https://bitbucket.org/alyssacliu/me405_termproject/src/master/Jacob's%20Code/TouchPanelDriver.py
'''

import pyb
import utime
from TouchPanelDriver import TouchPanelDriver

# Initialize the touch panel object

## The Pin representing ym
ym = pyb.Pin(pyb.Pin.cpu.A0)

## The Pin representing xm
Pin_xm = pyb.Pin(pyb.Pin.cpu.A1)
    
## The Pin representing yp
Pin_yp = pyb.Pin(pyb.Pin.cpu.A6)

## The Pin representing xp
Pin_xp = pyb.Pin(pyb.Pin.cpu.A7)

TP = TouchPanelDriver(Pin_xm, ym, Pin_xp, Pin_yp, .107, .187, [0, 0])

def avgtime():
    '''
    Finds the average run time of the following scan functions: Xscan, Yscan,
    Zscan and OptScan.
    '''
    # Test the average run time of each scan function
    xscan_initial = utime.ticks_us()
    for i in range(100):
        x = TP.Xscan()
    xscan_final = utime.ticks_us()
    xscan_time = utime.ticks_diff(xscan_final, xscan_initial)/100
    print('Average X Scan Time: ', str(xscan_time), ' microseconds')
        
    yscan_initial = utime.ticks_us()
    for j in range(100):
        y = TP.Yscan()
    yscan_final = utime.ticks_us()
    yscan_time = utime.ticks_diff(yscan_final, yscan_initial)/100
    print('Average Y Scan Time: ', str(yscan_time), ' microseconds')
        
    zscan_initial = utime.ticks_us()
    for k in range(100):
        z = TP.Zscan()
    zscan_final = utime.ticks_us()
    zscan_time = utime.ticks_diff(zscan_final, zscan_initial)/100
    print('Average Z Scan Time: ', str(zscan_time), ' microseconds')

    # Calculate total scan time of each scan function individually.
    Total_scantime = xscan_time+zscan_time+yscan_time
    print('Average total scan time: ', str(Total_scantime), 'microseconds')
    
    # Calculate total scan time of the optimized scan function.
    optscan_initial = utime.ticks_us()
    for l in range(100):
        pos = TP.OptScan()
    optscan_final = utime.ticks_us()
    optscan_time = utime.ticks_diff(optscan_final,optscan_initial)/100
    print('Average Optimal Scan Time: ', str(optscan_time), ' microseconds')
    
def calibrate():
    '''
    Calibrates the touch panel by having the user touch different parts of the 
    touch panel to approximate the center, height and width of the touch panel.
    '''
    print('Please touch the upper left hand corner of the touch panel.\r\n')
    print('(The corner closest to Motor 2)')
    z1 = False
    x1 = 0
    y1 = 0
    while z1 is False:
        # Scan for user input
        (z1, x1, y1) = TP.OptScan()
    # Now repeat for each corner
    print('Data point collected, please remove your finger.')
    print('x1 = ', str(x1), ' m')
    print('y1 = ', str(y1), ' m')
    utime.sleep(3)
    print('Please touch the upper right hand corner of the touch panel.')
    print('(The corner right above the Nucleo)')
    z2 = False
    x2 = 0
    y2 = 0
    while z2 is False:
        # Scan for user input
        (z2, x2, y2) = TP.OptScan()
    print('Data point collected, please remove your finger.')
    print('x2 = ', str(x2), ' m')
    print('y2 = ', str(y2), ' m')
    utime.sleep(3)
    print('Please touch the lower right hand corner of the touch panel.')
    print('(The corner just above the X5, X6, and X7 ports)')
    z3 = False
    x3 = 0
    y3 = 0
    while z3 is False:
        # Scan for user input
        (z3, x3, y3) = TP.OptScan()
    print('Data point collected, please remove your finger.')
    print('x3 = ', str(x3), ' m')
    print('y3 = ', str(y3), ' m')
    utime.sleep(3)
    print('Please touch the lower left hand corner of the touch panel.')
    print('(The corner closest to Motor 1)')
    z4 = False
    x4 = 0
    y4 = 0
    while z4 is False:
         # Scan for user input
         (z4, x4, y4) = TP.OptScan()
    print('Data point collected, please remove your finger.')
    print('x4 = ', str(x4), ' m')
    print('y4 = ', str(y4), ' m')
    utime.sleep(3)
    print('Now please press at the approximate center of the touch panel.')
    zc = False
    xc = 0
    yc = 0
    while zc is False:
        # Scan for user input
        (zc, xc, yc) = TP.OptScan()
        
    # There's a zero error in the data pad - the top right corner above the 
    # nucleo is the approximate location of the origin, so x2 and y2 are the 
    # 'zero error' points.
    
    xerror = x2
    yerror = y2
     
    # Now calculate a height and a width
    height = abs(((y3 + y4)/2) - ((y1 + y2)/2))
    width = abs(((x1)) - ((x2 + x3)/2))
    print('The height was calculated to be ', str(height), ' m.')
    print('The width was calculated to be ', str(width), ' m.')
    print('Please record these values for future reference.')
    print("It's recommended that this function be run at least 5 times to ")
    print("get proper approximations of height and width.")
     
    # Approximate the center of the touch panel
    xc_exp = width/2 + xerror
    yc_exp = height/2 + yerror
    print('Measured Geometric center at x = ', str(xc), ' m and y = ', str(yc), ' m.')
    print('Expected Geometric center at x = ', str(xc_exp), ' m and y = ', str(yc_exp), ' m.')
    percentdiffx = abs((xc - xc_exp)/((xc + xc_exp)/2))*100
    percentdiffy = abs((yc - yc_exp)/((yc + yc_exp)/2))*100
    print('Percent difference between Measured and Expected X center: ', str(percentdiffx), '%')
    print('Percent difference between Measured and Expected Y center: ', str(percentdiffy), '%')
    
def runindef():
    '''
    Collects Data indefinitely every 5 seconds and prints it to the console.
    '''
    # Output the Z, X and Y scans indefinitely at an interval of 5 seconds to
    # allow further testing of the touch pad.
    while True:
        pos = TP.OptScan()
        print('Z scan: ', str(pos[0]))
        print('X scan: ', str(pos[1]), ' m')
        print('Y scan: ', str(pos[2]), ' m')
        utime.sleep(5)
    
if __name__ == "__main__":
    # Test the touchpanel code from the driver
    # Compute Average Scan runtime
    avgtime()
    
    # Calibrate touch panel
    cal = True
    i = 0
    while cal:
        print('Running Calibration...')
        calibrate()
        i += 1
        x = input('Would you like to run another calibration test? (y/n)')
        if x == 'y':
            cal = True
        else:
            cal = False
            print('You ran ', str(i), ' tests.')
    
    r = input('Would you like to continue testing by running the scan indefinitely? (y/n)')
    
    if r == 'y':
        runindef()
    else:
        pass