'''
@file TouchPanelDriver.py
This class file is a driver for a resistive touch panel. The touch panel is 
meant to be rectanglular in shape and relatively small, but it could be 
modified to accept other simple geometric shapes for the touch pad. The 
functions contained within this driver include an x scan and a y scan that 
output the position of a single object in contact with the surface of the 
touch panel, a z scan that outputs a boolean signalling whether or not 
something is in contact with the touch panel, and an optimized combined scan
function that runs the x, y, and z scan as quickly as possible.

Documentation can be found here:
    https://bitbucket.org/alyssacliu/me405_termproject/src/master/Jacob's%20Code/TouchPanelDriver.py
'''

from pyb import Pin, ADC
import utime

class TouchPanelDriver():
    def __init__(self, pin_xm, pin_ym, pin_xp, pin_yp, height, width, center):
        '''
        Sets up the TouchPanelDriver object.
        @param pin_xm   The pin representing the negative x terminal on the 
                        touch pad.
        @param pin_ym   The pin representing the negative y terminal on the 
                        touch pad.
        @param pin_xp   The pin representing the positive x terminal on the 
                        touch pad.
        @param pin_yp   The pin representing the positive x terminal on the 
                        touch pad.
        @param height   The physical height of the touch pad in meters.
        @param width    The physical width of the touch pad in meters.
        @param center   The geometric center of the touch pad in meters, pass
                        in as a 2 element array: [x_center, y_center].
        '''
        ## Pin representing the negative x terminal
        self.Pin_xm = pin_xm 
        
        ## Pin representing the positive x terminal
        self.Pin_xp = pin_xp 
        
        ## Pin representing the negative y terminal
        self.Pin_ym = pin_ym 
        
        ## Pin representing the positive y terminal
        self.Pin_yp = pin_yp 
        
        ## The touch panel width
        self.width = width
        
        ## The touch panel height
        self.height = height
        
        ## The center of the touch panel
        self.center = center
        
    def Xscan(self):
        '''
        This method calculates the x position of an object in contact with the 
        surface of the touch panel. It reconfigures the pins representing the 
        xm, xp, yp and ym terminals to measure the voltage corresponding with 
        the position, and then it converts that voltage into a position in 
        meters.
        @return x_pos The x position in meters.
        '''
        # Reconfigure x terminals to high and low
        # Initialize pins to output
        self.Pin_xm.init(Pin.OUT_PP)
        self.Pin_xp.init(Pin.OUT_PP)
        # Set xm low
        self.Pin_xm.low()
        
        # Set xp high
        self.Pin_xp.high()

        # "Float" y terminals and reconfigure ym for analog input
        # Float yp
        self.Pin_yp.init(Pin.IN)

        # Configure ym for analog input
        ## ADC object reading from ym pin; it measures the voltage corresponding to x position
        self.adc_x = ADC(self.Pin_ym)
        
        # Read the voltage count from the ADC pin
        self.x_pos = self.adc_x.read()
        
        # Convert the voltage count value to position in meters
        self.x_pos = (self.x_pos/4095)*(self.width)-self.center[0]
        
        return self.x_pos

    def Yscan(self):
        '''
        This method calculates the y position of an object in contact with the 
        surface of the touch panel. It reconfigures the pins representing the 
        xm, xp, yp and ym terminals to measure the voltage corresponding with 
        the position, and then it converts that voltage into a position in 
        meters.
        @return y_pos The y position in meters.
        '''
        # Reconfigure y terminals to high and low
        # Initialize pins to output
        self.Pin_ym.init(Pin.OUT_PP)
        self.Pin_yp.init(Pin.OUT_PP)
        
        # Set ym low
        self.Pin_ym.low()
        
        # Set yp high
        self.Pin_yp.high()

        # "Float" x terminals and reconfigure ym for analog input
        # Float xp
        self.Pin_xp.init(Pin.IN)

        # Configure xm for analog input
        ## ADC object reading from ym pin; it measures the voltage corresponding to x position
        self.adc_y = ADC(self.Pin_xm)

        # Read the voltage count from the ADC pin
        self.y_pos = self.adc_y.read()
        
        # Convert the voltage count value to position in meters
        self.y_pos = (self.y_pos/4095)*(self.height)-self.center[1]
        
        return self.y_pos
        
    def Zscan(self):
        '''
        This method determines whether an object has made contact with the 
        touch panel or not.
        @return touch A boolean representing the state of the panel.
        '''
        # Reconfigure yp to be high and xm to be low
        # Initialize pins to output
        self.Pin_xm.init(Pin.OUT_PP)
        self.Pin_yp.init(Pin.OUT_PP)
        
        # Set ym low
        self.Pin_xm.low()
        
        # Set yp high
        self.Pin_yp.high()
        
        # Float ym
        self.Pin_ym.init(Pin.IN)
        
        # Reconfigure xp for analog input
        self.adc_1 = ADC(self.Pin_xp)        
         
        # Check if the values from the ADC pins are correct
        self.check1 = self.adc_1.read()
        
        if (self.check1>20):
            # Pin is not low; there is some intermediate voltage between xm and xp
            self.touch = True # ( ͡° ͜ʖ ͡°)
        else:
            self.touch = False 
        return self.touch

    def Scan(self):
        '''
        Finds the x and y position in addition to determining if an object has
        touched the panel. Returns the values as a tuple, with x position 
        first, then z scan, then y position.
        @return The tuple of the x, y and z scans.
        '''
        self.position = (self.Zscan(), self.Xscan(), self.Yscan())
        
        return self.position
    
    def OptScan(self):
        '''
        An optimized version of the Scan() function; it minimizes pin 
        reconfigurations to lower the code execution time.
        @return A tuple of the x and y positions and the z scan.
        '''
        # Begin with x scan
        
        # Reconfigure x terminals to high and low
        # Initialize pins to output
        self.Pin_xm.init(Pin.OUT_PP)
        self.Pin_xp.init(Pin.OUT_PP)
        
        # Set xm low
        self.Pin_xm.low()
        
        # Set xp high
        self.Pin_xp.high()

        # "Float" y terminals and reconfigure ym for analog input
        # Float yp
        self.Pin_yp.init(Pin.IN)

        # Configure ym for analog input
        ## ADC object reading from ym pin; it measures the voltage corresponding to x position
        self.adc_ym = ADC(self.Pin_ym)
        
        # Read the voltage count from the ADC pin
        self.x_pos = self.adc_ym.read()
        
        # Convert the voltage count value to position in meters
        self.x_pos = (self.x_pos/4095)*(self.width)-self.center[0]
        
        # Now do Z scan
        
        # Need yp high, xm low, float ym, xp adc 
        
        # Reconfigure yp to be high
        # Initialize pin to output
        self.Pin_yp.init(Pin.OUT_PP)
        
        # Set yp high
        self.Pin_yp.high()
        
        # Reconfigure xp for analog input
        self.adc_xp = ADC(self.Pin_xp)        
         
        # Check if the values from the ADC pins are correct
        self.check1 = self.adc_xp.read()
        
        if (self.check1>20):
            # Pin is not low; there is some intermediate voltage between xm and xp
            self.touch = True # ( ͡° ͜ʖ ͡°)
        else:
            self.touch = False 
            
        # Finally, run the y scan
        
        # Need ym low, yp high, float xm, adc xp
        
        # Reconfigure ym to be low
        self.Pin_ym.init(Pin.OUT_PP)
        self.Pin_ym.low()

        # Float xm
        self.Pin_xm.init(Pin.IN)

        # Read the voltage count from the ADC pin
        self.y_pos = self.adc_xp.read()
        
        # Convert the voltage count value to position in meters
        self.y_pos = (self.y_pos/4095)*(self.height)-self.center[1]

        return (self.touch, self.x_pos, self.y_pos)

if __name__ == "__main__":
    # Test the from the driver    
    ## The Pin representing ym
    Pin_ym = Pin(Pin.cpu.A0)

    ## The Pin representing xm
    Pin_xm = Pin(Pin.cpu.A1)

    ## The Pin representing yp
    Pin_yp = Pin(Pin.cpu.A6)

    ## The Pin representing xp
    Pin_xp = Pin(Pin.cpu.A7)

    TP = TouchPanelDriver(Pin_xm, Pin_ym, Pin_xp, Pin_yp, .107, .187, [.0913, .0504])
    
    # Test the average run time of each scan function
    xscan_initial = utime.ticks_us()
    for i in range(100):
        x = TP.Xscan()
    xscan_final = utime.ticks_us()
    xscan_time = utime.ticks_diff(xscan_final, xscan_initial)/100
    print('Average X Scan Time: ', str(xscan_time), ' microseconds')
        
    yscan_initial = utime.ticks_us()
    for j in range(100):
        y = TP.Yscan()
    yscan_final = utime.ticks_us()
    yscan_time = utime.ticks_diff(yscan_final, yscan_initial)/100
    print('Average Y Scan Time: ', str(yscan_time), ' microseconds')
        
    zscan_initial = utime.ticks_us()
    for k in range(100):
        z = TP.Zscan()
    zscan_final = utime.ticks_us()
    zscan_time = utime.ticks_diff(zscan_final, zscan_initial)/100
    print('Average Z Scan Time: ', str(zscan_time), ' microseconds')

    # Calculate total scan time of each scan function individually.
    Total_scantime = xscan_time+zscan_time+yscan_time
    print('Average total scan time: ', str(Total_scantime), 'microseconds')
    
    # Calculate total scan time of the optimized scan function.
    optscan_initial = utime.ticks_us()
    for l in range(100):
        pos = TP.OptScan()
    optscan_final = utime.ticks_us()
    optscan_time = utime.ticks_diff(optscan_final,optscan_initial)/100
    print('Average Optimal Scan Time: ', str(optscan_time), ' microseconds')

    # Calibrate the touch panel


    print('Please touch the upper left hand corner of the touch panel.\r\n')
    print('(The corner closest to Motor 2)')
    z1 = False
    x1 = 0
    y1 = 0
    while z1 is False:
        # Scan for user input
        (z1, x1, y1) = TP.OptScan()
    # Now repeat for each corner
    print('Data point collected, please remove your finger.')
    print('x1 = ', str(x1), ' m')
    print('y1 = ', str(y1), ' m')
    utime.sleep(3)
    print('Please touch the upper right hand corner of the touch panel.')
    print('(The corner right above the Nucleo)')
    z2 = False
    x2 = 0
    y2 = 0
    while z2 is False:
        # Scan for user input
        (z2, x2, y2) = TP.OptScan()
    print('Data point collected, please remove your finger.')
    print('x2 = ', str(x2), ' m')
    print('y2 = ', str(y2), ' m')
    utime.sleep(3)
    print('Please touch the lower right hand corner of the touch panel.')
    print('(The corner just above the X5, X6, and X7 ports)')
    z3 = False
    x3 = 0
    y3 = 0
    while z3 is False:
        # Scan for user input
        (z3, x3, y3) = TP.OptScan()
    print('Data point collected, please remove your finger.')
    print('x3 = ', str(x3), ' m')
    print('y3 = ', str(y3), ' m')
    utime.sleep(3)
    print('Please touch the lower left hand corner of the touch panel.')
    print('(The corner closest to Motor 1)')
    z4 = False
    x4 = 0
    y4 = 0
    while z4 is False:
         # Scan for user input
         (z4, x4, y4) = TP.OptScan()
    print('Data point collected, please remove your finger.')
    print('x4 = ', str(x4), ' m')
    print('y4 = ', str(y4), ' m')
    utime.sleep(3)
    print('Now please press at the approximate center of the touch panel.')
    zc = False
    xc = 0
    yc = 0
    while zc is False:
        # Scan for user input
        (zc, xc, yc) = TP.OptScan()
    
    # There's a zero error in the data pad - the top right corner above the 
    # nucleo is the approximate location of the origin, so x2 and y2 are the 
    # 'zero error' points.
    
    xerror = x2
    yerror = y2
     
    # Now calculate a height and a width
    height = abs(((y3 + y4)/2) - ((y1 + y2)/2))
    width = abs(((x1)) - ((x2 + x3)/2))
    print('The height was calculated to be ', str(height), ' m.')
    print('The width was calculated to be ', str(width), ' m.')
    print('Please record these values for future reference.')
    print("It's recommended that this function be run at least 5 times to ")
    print("get proper approximations of height and width.")
     
    # Approximate the center of the touch panel
    xc_exp = width/2 + xerror
    yc_exp = height/2 + yerror
    print('Measured Geometric center at x = ', str(xc), ' m and y = ', str(yc), ' m.')
    print('Expected Geometric center at x = ', str(xc_exp), ' m and y = ', str(yc_exp), ' m.')
    percentdiffx = abs((xc - xc_exp)/((xc + xc_exp)/2))*100
    percentdiffy = abs((yc - yc_exp)/((yc + yc_exp)/2))*100
    print('Percent difference between Measured and Expected X center: ', str(percentdiffx), '%')
    print('Percent difference between Measured and Expected Y center: ', str(percentdiffy), '%')

    # Output the Z, X and Y scans indefinitely at an interval of 5 seconds to
    # allow further testing of the touch pad.
    while True:
        pos = TP.OptScan()
        print('Z scan: ', str(pos[0]))
        print('X scan: ', str(pos[1]))
        print('Y scan: ', str(pos[2]))
        utime.sleep(5)