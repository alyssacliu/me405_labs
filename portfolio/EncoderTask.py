'''
@file   EncoderTask.py
@brief  A task that controls the encoders corresponding to the platform system.
@details    This task implements EncoderDriver.py to read from the encoders in 
            the balancing platform system and to provide feedback for  
            ControllerTask.py using Queues to share the data. \ref
@author Jacob Burghgraef & Alyssa Liu
@date June 11th, 2021

A state transition diagram for the Finite State Machine (FSM) implemented in 
this task is shown below:
    
@image html "ME 405 Term Project Encoder Task FSM.PNG"

The documentation for this file can be found here:
    https://bitbucket.org/alyssacliu/me405_termproject/src/master/EncoderTask.py
'''

from pyb import Pin, Timer
from EncoderDriver import EncoderDriver
import TPShares

class EncoderTask:
    '''
    @brief      Reads data from the encoders and sends them to TPShares.py
    @details    This task initializes by setting up two encoder objects from 
                the EncoderDriver class to represent the encoders in the 
                balancing platform system using timers 4 and 8 and pins B6, B7,
                C6 and C7. Then, it waits for the command to begin balancing 
                in an idle state. Upon receiving this command from the UI task,
                it zeros the encoders at the current position and then 
                transitions to the data collection state. In the data 
                collection state, the task reads the current angle from the
                encoders, calculates the angular velocities, and then writes
                these values to the shared variables sent to the controller 
                and data queues used for data collection. If a halt command 
                from the user is detected, the task will then transition from 
                the data collection state to the idle state.
    '''
    
    def __init__(self, interval):
        '''
        @brief  Sets up the Encoders and the general encoder task.
        @param interval The interval at which the FSM runs, this is used for 
                        speed approximations inside the corresponding encoder
                        driver file; input in units of milliseconds.
        '''
        # Define system constants
    
        ## The side length of the platform in meters, used to convert Motor Angle
        #  to platform angle
        self.lp = .1
    
        ## The length of the motor arm in meters, used to convert Motor Angle to
        #  platform angle
        self.rm = .06            
        
        ## The state of the Finite State Machine
        self.state = 0
        
        ## The interval at which the FSM runs, used in the encoder driver to 
        #  calculate the angular velocity. This value is converted to 
        #  microseconds for the calculation in the encoder driver.
        self.interval = interval*1000
        
        # Create Encoder 1, used to measure rotation about the X axis
        ## Pin 1 of Encoder 1
        self.PB6 = Pin(Pin.cpu.B6)
        ## Pin 2 of Encoder 1
        self.PB7 = Pin(Pin.cpu.B7)
        ## The timer for Encoder 1
        self.tim1 = Timer(4, prescaler = 0, period = 0xFFFF)
        ## Encoder 1, directly measuring rotation about the Motor 1's Y axis,
        #  and indirectly measuring rotation about the platform's X axis
        self.Enc1 = EncoderDriver(self.PB6, self.PB7, self.tim1, self.interval)
        
        # Create Encoder 2, used to measure rotation about the Y axis
        ## Pin 1 of Encoder 1
        self.PC6 = Pin(Pin.cpu.C6)
        ## Pin 2 of Encoder 2
        self.PC7 = Pin(Pin.cpu.C7)
        ## The timer for Encoder 2
        self.tim2 = Timer(4, prescaler = 0, period = 0xFFFF)
        ## Encoder 2, directly measuring rotation about the Motor 2's X axis,
        #  and indirectly measuring rotation about the platform's Y axis
        self.Enc2 = EncoderDriver(self.PC6, self.PC7, self.tim2, self.interval)
        
    def run(self):
        '''
        @brief  Runs 1 iteration of the Encoder Task.
        '''
        if self.state == 0:
            # Initialization State, moves to idle state
            self.state = 1
        
        elif self.state == 1:
            # Idle state, waits for the cmd from the UI that the platform has 
            # been stabilized, and that balancing can begin
            
            if TPShares.begbal.get() == 1:
                # Zero out the Encoder position
                self.Enc1.set_position(0)
                self.Enc2.set_position(0)
                
                # Go to reading/writing state 
                self.state = 2
                
        elif self.state == 2:
            # Data Collection state, reads from the encoders and then writes 
            # that data to shared variables and queues
            
            if TPShares.halt.get() == 1:
                # System has been told to halt, go back to the idle state
                self.state = 1
            else:
                # Reading from encoders/writing to shares and queues state
                
                # Read angles from the encoders and convert them to platform angles
                th_x = self.Enc1.get_position()*(-self.rm/self.lp) # in radians
                th_y = self.Enc2.get_position()*(-self.rm/self.lp) # in radians
                
                # Read deltas from the encoders and use them to get platform 
                # angular velocities.
                th_xdot = self.Enc1.getSpeed()*(-self.rm/self.lp) # in radians per second
                th_ydot = self.Enc2.getSpeed()*(-self.rm/self.lp) # in radians per second
    
                # Put these values into the shared data variables and data 
                # collection queues
                TPShares.th_x.put(th_x)
                TPShares.th_y.put(th_y)
                TPShares.th_xdot.put(th_xdot)
                TPShares.th_ydot.put(th_ydot)
                TPShares.th_xdc.put(th_x)
                TPShares.th_ydc.put(th_y)
                TPShares.th_xdotdc.put(th_xdot)
                TPShares.th_ydotdc.put(th_ydot)
            
        yield(self.state)