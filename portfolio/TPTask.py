'''
@file       TPTask.py
@brief      Implements the touch panel driver into a cooperative task.
@details    This task works by initializing a touch panel object using the 
            TouchPanelDriver(), and using this object, the task measures 
            position data from an object in contact with the touch panel. Note 
            that new data points are only collected when there is contact with 
            the touch panel. Additionally, the last x and y positions are 
            stored internally to find the difference in position between data 
            points, which is divided by the interval to approximate velocities.
@author Jacob Burghgraef & Alyssa Liu
@date June 11th, 2021

A state transition diagram for the Finite State Machine (FSM) implemented in 
this task is shown below:
    
@image html "ME 405 Term Project TPTask FSM.PNG"

The documentation for this file can be found here:
    https://bitbucket.org/alyssacliu/me405_termproject/src/master/TPTask.py
'''

import TPShares
from TouchPanelDriver import TouchPanelDriver
from pyb import Pin

class TPTask():
    '''
    @brief      Responsible for position, velocity, and contact measurements.
    @details    This task works cooperatively with the other tasks in the 
                balancing platform system. It has two states: an idle state 
                and a measurement state. The idle state is used when the 
                system is in a fault state and before data collection has 
                begun. The measurement state measures the position from the 
                TouchPanelDriver() and uses that to calculate position and 
                velocity data for the closed loop feedback in the controller. 
                Additionally, whenever a fault is detected in the measurement
                state, the task transitions back to the idle state.
    '''
    def __init__(self, interval):
        '''
        @brief          Creates a TPTask object.
        @param interval The interval at which the task runs, used internally 
                        for velocity calculations. Pass this in milliseconds.
        '''
        ## The state of the Finite State Machine
        self.state = 0
        
        ## The interval at which the task runs, used for velocity calculation.
        #  The parameter is given in milliseconds, so the internal variable 
        #  used by this class is converted to seconds.
        self.interval = interval/1000
        
        ## The Pin representing ym
        self.Pin_ym = Pin(Pin.cpu.A0)

        ## The Pin representing xm
        self.Pin_xm = Pin(Pin.cpu.A1)

        ## The Pin representing yp
        self.Pin_yp = Pin(Pin.cpu.A6)

        ## The Pin representing xp
        self.Pin_xp = Pin(Pin.cpu.A7)

        ## The object representing the Touch Panel
        self.TchPnl = TouchPanelDriver(self.Pin_xm, self.Pin_ym, self.Pin_xp, 
                                   self.Pin_yp, .107, .187, [.0913, .0504])
        
        ## The boolean representing whether an object is in contact with the 
        #  Touch Panel or not, this is the first value read in position
        self.z = False
        
        ## The current x position of the ball
        self.x = 0
        
        ## The most recent x position of the ball, used for velocity calculation
        self.x_last = 0
        
        ## The current y position of the ball
        self.y = 0
        
        ## The most recent y position of the ball, used for velocity calculation
        self.y_initial = 0
        
    def run(self):
        if self.state == 0:
            # Initialization State, moves on to idle state.
            self.state = 1
            
        elif self.state == 1:
            # Idle State, waits for a command from the UITask before reading
            # from the touch panel.
            if TPShares.begbal.get() == 1:
                self.state = 2
            
        elif self.state == 2:
            # This is the state that writes measured position and velocity 
            # data to the shared variables file TPShares. Another state should
            # be added if calibration is desired.
            
            if TPShares.halt.get() == 1:
                # Go back to idle state
                self.state = 1
            else:
                # Check if the ball is touching the touch panel, first read from 
                # the optimized scan, and then check the value of z
                (self.z, self.x, self.y) = self.TchPnl.OptScan()
                
                if self.z:
                    # If something is in contact with the touch panel, then 
                    # update the shared variables and data queues.
                    TPShares.x.put(self.x)
                    TPShares.y.put(self.y)
                    TPShares.x_dot.put((self.x - self.x_last)/(self.interval))
                    TPShares.y_dot.put((self.y - self.y_last)/(self.interval))
                    TPShares.xdc.put(self.x)
                    TPShares.ydc.put(self.y)
                    TPShares.x_dotdc.put((self.x - self.x_last)/(self.interval))
                    TPShares.y_dotdc.put((self.y - self.y_last)/(self.interval))
                    # Update the last position measurement
                    self.x_last = self.x
                    self.y_last = self.y
        else:
            # Error State
            pass
        
        yield(self.state)