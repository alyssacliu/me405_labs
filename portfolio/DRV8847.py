'''
@file       DRV8847.py
@brief      A Motor driver file designed to control DRV8847 motors.
@details    This Motor driver emulated the Timer class found in the 
            micropython pyb module in that a DRV8847 object is set up to 
            handle fault detection, enabling and disabling the motors, and 
            setting up the motor channels that control the motors directly.
            The motor channels handle to initialization of the timer channels 
            that control the motor with PWM, and they accept varying PWM % 
            values.
@author Jacob Burghgraef & Alyssa Liu
@date June 11th, 2021

The documentation for this file can be found here:
    https://bitbucket.org/alyssacliu/me405_termproject/src/master/DRV8847.py
'''
from pyb import Pin, Timer, ExtInt
import utime, micropython

class DRV8847:
    '''
    @brief      Handles the overhead of driving the motors.
    @details    The Motor driver class that handles fault detection, enabling 
                and disabling motors, and setting up the motor channels. Use 
                this class as you would use the timer class in the pyb module. 
                That is, use this class to configure the settings on the motor
                and to initialize motor channels.
    '''
    def __init__(self, nSLEEP_pin, nFAULT_pin):
        '''
        @brief              Sets up the DRV8847 object.
        @param  nSLEEP_pin  The pin that represents the sleep state of the 
                            motor, it can turn the motor on and off. 
        @param  nFAULT_pin  The pin that represents the fault state of the 
                            motor, it halts the motor until the fault is 
                            cleared.
        '''
        ## The pin representing the sleep state of the motor
        self.nSLEEP = nSLEEP_pin
        
        ## The pin representing the fault state of the motor
        self.nFAULT = nFAULT_pin
        
        # Begin with the motors turned off
        self.nSLEEP.low()
        
        ## The flag representing the state of the fault pin
        self.fault = False
        
        ## The interrupt used for fault handling
        ExtInt(self.nFAULT, ExtInt.IRQ_FALLING, Pin.PULL_NONE, self.fault_CB)
        
        # For keeping track of interrupt errors
        micropython.alloc_emergency_exception_buf(100)
        
    def enable(self):
        '''
        @brief  Enables the motor by setting the nSLEEP pin high.
        '''
        self.nSLEEP.high()
        
    def disable(self):
        '''
        @brief  Disables the motor by setting the nSLEEP pin low.
        '''
        self.nSLEEP.low()
        
    def fault_CB (self, faultPin):
        '''
        @brief          The callback referenced when an interrupt occurs.
        @param faultPin The pin object representing the nFAULT pin.
        '''
        # Set the flag representing the fault state to true
        self.fault = True
    
    def clear_fault(self):
        '''
        @brief  Clears the fault state from the motor.
        '''
        # Set the fault pin to low
        self.nFAULT.low()
        
        # Set the flag representing the fault state to False
        self.fault = False
    
    def channel(self, INx_pin, INy_pin, INxy_timer, ch1, ch2):
        '''
        @brief              Creates a motor channel object.
        @param INx_pin      The pin representing the negative terminal on the 
                            motor.
        @param INy_pin      The pin representing the positive terminal on the 
                            motor.
        @param INxy_timer   The timer object used to control PWM in the motor 
                            channel.
        @param ch1          An unsigned integer between 1 and 4 representing 
                            the timer channel number used for PWM control. 
                            This is usually 1 or 3, since it is paired with 
                            ch2.
        @param ch2          An unsigned integer between 1 and 4 representing 
                            the timer channel number used for PWM control. 
                            This is usually 2 or 4, since it is paired with 
        '''    
        return DRV8847_channel(INx_pin, INy_pin, INxy_timer, ch1, ch2)
    
class DRV8847_channel:
    
    def __init__(self, INx_pin, INy_pin, INxy_timer, ch1, ch2):
        '''
        @brief              Creates a motor channel object.
        @details            The class that drives the motors; its sole purpose
                            is to use the PWM value to set the duty cycle on a
                            single motor.
        @param INx_pin      The pin representing the negative terminal on the 
                            motor.
        @param INy_pin      The pin representing the positive terminal on the 
                            motor.
        @param INxy_timer   The timer object used to control PWM in the motor 
                            channel.
        @param ch1          An unsigned integer between 1 and 4 representing 
                            the timer channel number used for PWM control. 
                            This is usually 1 or 3, since it is paired with 
                            ch2.
        @param ch2          An unsigned integer between 1 and 4 representing 
                            the timer channel number used for PWM control. 
                            This is usually 2 or 4, since it is paired with 
                            ch1.
        '''
        ## The Pin representing the negative terminal
        self.INx_pin = INx_pin
        
        ## The Pin representing the positive terminal
        self.INy_pin = INy_pin
        
        ## The timer object used for PWM
        self.timer = INxy_timer
        
        self.timch1 = self.timer.channel(ch1, Timer.PWM, pin = self.INx_pin)
        self.timch2 = self.timer.channel(ch2, Timer.PWM, pin = self.INy_pin)
        
    def set_level(self, level):
        '''
        @brief      Sets the PWM level for a selected motor channel.
        @details    The PWM level set for the motor channel is filtered such 
                    that positive values spin the motor CCW and negative 
                    values spin the motor CW. Note that values greater than 
                    100 are automatically set to 100 on the positive terminal,
                    and negative values less than -100 are automatically set 
                    to 100 on the negative terminal.
        @param      A signed floating point number representing the desired 
                    duty cycle percentage to drive the motor.
        '''
        if level >= 0 and level <= 100:
            # Spin the Motor in one direction or not at all
            self.timch2.pulse_width_percent(level)
            self.timch1.pulse_width_percent(0)
        
        elif level < 0 and level >= -100:
            # Spin the Motor in the opposite direction
            self.timch2.pulse_width_percent(0)
            self.timch1.pulse_width_percent(abs(level))
        
        elif level > 100:
            # Saturate the motor
            self.timch2.pulse_width_percent(100)
            self.timch1.pulse_width_percent(0)
            
        else:
            # Level is below -100%, saturate the negative terminal
            self.timch2.pulse_width_percent(0)
            self.timch1.pulse_width_percent(100)
    
if __name__ == "__main__":
    # Test DRV8847 class code
    
    # Create the nSLEEP and nFAULT pins
    nSLEEP_pin = Pin(Pin.cpu.A15, Pin.OUT_PP)
    nFAULT_pin = Pin(Pin.cpu.B2)
    
    # Create the Motor Driver Object the will control both motors
    DRV8847 = DRV8847(nSLEEP_pin, nFAULT_pin)
    
    # The timer to be used for controlling the Motors via PWM
    tim = Timer(3, freq = 20000);
    
    # Create an object representing Motor 1
    
    # Input Pins
    IN1_pin = Pin(Pin.cpu.B4)  # Corresponds to OUT1/M1-
    IN2_pin = Pin(Pin.cpu.B5)  # Corresponds to OUT2/M1+

    # The motor channel that controls motor 1
    Mot1 = DRV8847.channel(IN1_pin, IN2_pin, tim, 1, 2)    
    
    # Create an object representing Motor 2
    
    # Input Pins
    IN3_pin = Pin(Pin.cpu.B0)  # Corresponds to OUT3/M2-
    IN4_pin = Pin(Pin.cpu.B1)  # Corresponds to OUT4/M2+
    
    # The motor channel that controls motor 2
    Mot2 = DRV8847.channel(IN3_pin, IN4_pin, tim, 3, 4)
    
    # Now Enable the Motor
    DRV8847.enable()
    
    # The following code tests the motors at a variety of velocities for 2 
    # seconds at each velocity. Velocities include saturated PWM values, 
    # negative PWM values, and low PWM values.
    
    # Test the first motor
    Mot1.set_level(100)
    
    utime.sleep(2)
    
    Mot1.set_level(50)
    
    utime.sleep(2)
    
    Mot1.set_level(500)
    
    utime.sleep(2)
    
    Mot1.set_level(0)
    
    utime.sleep(2)

    Mot1.set_level(-500)
    
    utime.sleep(2)
    
    Mot1.set_level(-50)
    
    utime.sleep(2)
    
    Mot1.set_level(-100)
    
    utime.sleep(2)
    
    # Set the first motor to have no output
    Mot1.set_level(0)
    
    utime.sleep(2)
    
    # Test the second motor
    Mot2.set_level(100)
    
    utime.sleep(2)
    
    Mot2.set_level(50)
    
    utime.sleep(2)
    
    Mot2.set_level(500)
    
    utime.sleep(2)
    
    Mot2.set_level(0)
    
    utime.sleep(2)

    Mot2.set_level(-500)
    
    utime.sleep(2)
    
    Mot2.set_level(-50)
    
    utime.sleep(2)
    
    Mot2.set_level(-100)
    
    utime.sleep(2)
    
    # Set the second motor to have no output
    Mot2.set_level(0)
    
    # Test Fault Detection
    
    Mot1.set_level(100) # Set Motor 1 at Max Velocity
    
    start = utime.ticks_us() 
    
    curr = utime.ticks_us()
    
    # Wait 10 seconds for the user to saturate the motor and trigger fault
    # detection.
    while(utime.ticks_diff(curr,start) < 10^7):
        curr = utime.ticks_us()
        if (DRV8847.fault):
            DRV8847.nFAULT.high()
            break;
    
    # Set fault pin low
    DRV8847.nFAULT.low()
    
    # Set motor speed to zero
    Mot1.set_level(0)
    
    # Finally, disable the motor
    DRV8847.disable()