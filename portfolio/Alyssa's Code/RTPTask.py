'''
@file RTPTask.py

@author Alyssa Liu

@date May 31st, 2021

@brief This file is the task script for the RTP, resistive touch panel.

@details The following script outlines the states and tasks for the RTP, resistive touch panel.
'''

import pyb 
import TPShares, utime


class RTPTask():
	
	init_state0 = 0

	initalPosition_state1 = 1

	RTPCalculation_state2 = 2

	def __init__(self, RTP, interval):		

		self.interval = interval

		self.state = 0

		self.RTP = RTP

		self.x = 0

		self.y = 0

		self.K1 = -5.7941

		self.K2 = -1.5335

		self.K3 = -1.5612

		self.K4 = -0.1296

		self.R = 2.21

		self.Kt = 13.8/1000

		self.Vdc = 12

		self.start_time = utime.ticks_us()

		self.next_interval = self.start_time + self.interval

	def run(self):
		if ((utime.ticks_diff(self.start_time, self.next_interval)) >= 0):
			if (self.state == self.init_state0):
				self.x = TPShares.x.get()
				self.y = TPShares.y.get()

				self.x_inital = 0
				self.y_inital = 0
				pyb.delay(1000)

			elif (self.state == self.initalPosition_state1):
				self.x_inital = self.x
				self.y_inital = self.y

                position = self.RTP.OptScan()
				self.x = position[1]
				self.y = position[2]

				self.state = self.RTPCalculation_state2
			
			elif (self.state == self.RTPCalculation_state2):
				TPShares.x.put(self.x)
                TPShares.y.put(self.y)
                TPShares.xdot.put((self.x - self.x_old) * 1000 / self.interval)
				TPShares.ydot.put((self.y - self.y_old) * 1000 / self.interval)

			else:
				pass

			self.next_interval = self.start_time + self.interval

			yield(self.state)

