'''
@file Lab0x03

@page Lab0x03

@section sec_title3 Lab0x03: Pushing the Right Buttons

@author Jacob Burghgraef

@date May 5th, 2021

@section sec_overview Overview

This lab measures the step response of the RC circuit connected to the USER 
button on the Nucleo. The pin representing the state of the USER button, 
Pin C13, is shorted to a pin that accepts analog input, Pin A0. From there, 
the ADC class in the pyb module is used to configure Pin A0 to read the 
voltage output from the 12-bit Successive Approximation Register (SAR) on the 
Nucleo. This voltage output is in the form of a discrete integer between 0 and 
4095, and represents a fraction of the DC power supply voltage (3.3 V). This 
output is converted to units of Volts in the frontend using 
(output)*(3.3 V)/4095.  From there, the voltage is plotted against time in 
milliseconds to yield a step response with a time constant of approximately 
0.5 ms. Additionally, the data (timestamp and voltage integers) are exported 
as a csv for the convenience of further data analysis. Finally, the timestamps
are estimated for the voltage data by assuming each voltage data point is 
sampled exactly 20 microseconds apart. The data buffer array storing the 
voltage integers samples data at 50kHz to get about 500 data points, which 
corresponds to about 5 ms of data - plenty of time to measure the step 
response, which reaches steady state in approximately 4 to 5 times the time
constant, or about 2 to 2.5 ms. For more information on the implementation of 
this data capture process, please see Frontend.py and Lab3main.py \ref

@section sec_results Results

@image html "Voltage Plot.png"
Figure_1 - The raw Voltage vs Time data outputted by the frontend.py file.

Data in Figure 1 was plotted using the matplotlib module, and displays the 
voltage integer data converted to units of volts on the y-axis and the 
corresponding timestamp on the x-axis. Note the time axis is shifted to the
right by 3 seconds to account for delays between the time when the user starts 
collecting data and when the user presses the button.

@image html "Linearized Voltage Plot.JPG"
Figure_2 - The Voltage data linearized and fit with a linear best-fit line.

Figure_2 was generated in excel, and shows the linearized solution to 
the first-order ODE that represents a capacitor charging in an RC circuit. 
Note the only operation done to the time axis is truncating data where no 
response occured and setting the first data point a time t=0. The slope in the
best fit equation, 2.0098, is the inverse of the time constant, which when 
used to find the time constant yields about 0.498 ms. The y-intercept is 
merely a constant of integration from the solution to the ODE.

Note that there are 3 valid methods to approximate the time constant:
    1. Find the value of the time constant at 63.2% of 3.3V
    2. Find the tangent line the beginning of the step response and then find 
    the time at which the tangent line crosses 3.3V
    3. Linearize the differential equation and find a best fit to get the 
    time constant
    
The first two methods are only helpful when trying to find the time constant 
graphically. The third method is most helpful when approximating the time 
constant numerically, as was done in this lab. Thus, the third method was used 
instead of the first two methods, and it yielded good results. Given the high 
R-squared value, the data certainly follows a linear trend, which suggests the 
model should yield an accurate time constant.

@image html "Time Constant Table.JPG"
Table_1 - Comparison of Time Constants

Table_1 shows the theoretical time constant value calculated from the reported 
resistance (~4.8 kOhms) and capacitance (~100nF) Nucleo data sheet, and the 
experimental time constant is the time constant calculated in this lab. 
Considering the ADC is accurate to <5%, the 3.6% difference is reasonable, and 
validates not only the experimental model, but also the reported values on the 
data sheet.
'''