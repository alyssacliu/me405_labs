'''
@file Encoder_Driver_Lab6.py

@author Jacob Burghgraef

@date December 4th, 2020

@brief This file is the driver for an encoder

@details This file is the driver for an encoder; it can retrieve the encoder's 
         current position, set the encoder's current position to a certain 
         value, update the current position, and retrieve the difference 
         between the current position and the most recent position.

The documentation can be found here: https://bitbucket.org/jburghgraef/me305_labs/src/master/Lab%206/Encoder_Driver_Lab6.py
'''

import pyb

class EncoderDriver:
    '''
    @brief      A driver class with a few basic functions for an encoder.
    @details    This driver class uses the pyb pin function and timer function
                to create a driver for an encoder. The functions within this 
                driver include update(), which reads the encoder's position, 
                getposition(), which retrieves the encoder's positon, 
                setposition(), which sets the encoder's position, and 
                getdelta(), which retrieves the difference between the two
                most recent position values in update.
    '''
    
    def __init__ (self, pin1, pin2, timer, interval):
        '''
        @brief      Creates an encoderdriver object.
        @details    This constructor sets up the encoderdriver object by 
                    defining the pin the encoder connects to and the timer
                    channel to be used in reading from the encoder.
        @param      pin 1 The 'A' pin for the encoder
        @param      pin 2 The 'B' pin for the encoder
        @param      timer The timer for the encoder
        @param      interval The interval at which the corresponding FSM is 
                    running, used in calculating the angular velocity of the 
                    motor.
        '''
        
        ## Create 2 'pin' variables representing the pins the encoder connects to on the Nucleo
        self.pin1 = pin1
        self.pin2 = pin2

        ## The pyb timer object used for the encoder
        self.tim = timer
        self.tim.init(prescaler = 0, period = 0xFFFF)
        
        ## Initialize timer channels     
        self.tim.channel(1, pin = self.pin1, mode = pyb.Timer.ENC_AB)
        self.tim.channel(2, pin = self.pin2, mode = pyb.Timer.ENC_AB)
        
        ## The interval used in the measured speed approximation
        self.interval = interval
        
        ## Position of encoder - initialized at zero
        self.position = 0
        
        ## Storage variable for calculation of the delta value - initialized at zero
        self.old_position = 0
        
        ## Represents the output of the delta calculation - initialized at zero
        self.delta = 0
        
        ## Corrected delta value - initialized at zero
        self.corr_delta = 0
        
        ## Represents the approximate angular velocity of the motor
        self.speed = 0
        
    def update(self):
        '''
        @brief      Obtains the current reading from the Encoder.
        @details    This obtains the current reading from the encoder, and
                    it calculates the change in position between the current
                    reading and the most recent reading, otherwise known as 
                    the 'delta.' update() then checks for overflow and 
                    underflow in the delta values, and then corrects it. 
                    Additionally, it calculates the speed of the motor.
        '''  
        
        # Set the old position to the position from the last loop
        self.old_position = self.position
        
        # Set the position to the current counter reading
        self.position = self.tim.counter()
        
        # Calculate delta
        self.delta = self.position - self.old_position
        
        # Check for Overflow and Underflow
        if (self.delta == 0):
            self.delta = 0;
        elif(self.delta < (-0xFFFF/2)):
            # Correct Overflow
            self.delta = (0xFFFF - self.old_position) + self.position
        elif (self.delta > (0xFFFF/2)):
            # Correct Underflow
            self.delta = -((0xFFFF - self.position) + self.old_position)
        else:
            pass
        
        # Approximate the speed in RPMs
        self.speed = (self.delta*60000000)/(4000*self.interval)
    
    def get_position(self):
        '''
        @brief      Obtains the position from update() and returns the position.
        @return     An integer value representing the current position of
                    the encoder.
        '''  
        return int(self.position)
    
    def set_position(self, pos):
        '''
        @brief      Sets current position of Encoder to a specified value.
        @param      The user inputted position.
        '''  
        self.position = self.tim.counter(pos)
        
    def get_delta(self):
        '''
        @brief      Obtains the delta value.
        @details    Obtains the delta value calculated and checked in update().
        @return     Integer value of delta.
        '''      
        return self.delta
    
    def getSpeed(self):
        '''
        @brief      Obtains the speed value calculated and checked in update().
        @return     Floating point value of speed
        '''
        return self.speed