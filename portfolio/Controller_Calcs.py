'''
@file Controller_Calcs.py

This file covers the calculations for the controller task, copy and paste the 
code inside the "ctrlcalc" function or the entire function itself into your 
controller task. Be sure to add the 'self' where appropriate when adapting 
this to your controller task object.
'''

import TPShares

def ctrlcalc():
    
    # Retrieve the values we need from TPshares
    
    # The following comes from the Touch Panel Task
    x = TPShares.x.get()
    y = TPShares.y.get()
    xdot = TPShares.xdot.get()
    ydot = TPShares.ydot.get()
    
    # The following comes from the Encoder Task
    th_x = TPShares.th_x.get()
    th_y = TPShares.th_y.get()
    th_xdot = TPShares.th_xdot.get()
    th_ydot = TPShares.th_ydot.get()
    
    # Now that we have all the values we need, let's calculate our torque value
    # My gain values from HW0x05 were K = [-5.7941, -1.5335, -1.5612, -0.1296]
    
    # Make these constants
    K1 = -5.7941
    K2 = -1.5335
    K3 = -1.5612
    K4 = -0.1296
    
    ## The torque value we need to drive Motor X using a simplified dot product
    Tx = K1*x + K2*th_y + K3*xdot + K4*th_ydot
    
    ## Now calculate the torque for Motor Y
    Ty = K1*y + K2*th_x + K3*ydot + K4*th_xdot
    
    # Now that we have Tx and Ty, we need to convert them to % duty cycles
    
    # From the Motor Data Sheet,
    R = 2.21        # Terminal resistance, in ohms
    Kt = 13.8/1000  # Torque constant, in Nm/A
    Vdc = 12        # Motor voltage supply, in volts
    
    # Now, let's calculate the Dty Cycle %'s
    
    dtyX = ((100*R)/(4*Kt*Vdc))*Tx
    dtyY = ((100*R)/(4*Kt*Vdc))*Ty
    
    # Finally, put the X and Y duty cycle percentages into the shares file
    TPShares.dtyX.put(dtyX)
    TPShares.dtyY.put(dtyY)