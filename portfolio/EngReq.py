"""
@file EngReq.py

@subpage ME405ERQ Requirements

@section sec_titleERQ Engineering Requirements 

@author Jacob Burghgraef and Alyssa Liu

@date June 11th, 2021

@section sec_overview Overview

There were 3 different drivers that needed to be made for the balancing system; 
these include a touch panel driver, a motor driver, and an encoder driver.

@section sec_RTP Resistive Touch Panel Driver Requirements

    1. A constructor that requires input parameters for four arbitrary pin 
    objects representing xp, xm, yp, and ym as well as additional numerical 
    parameters representing the width and length of the resistive touch panel 
    and the coordinate representing the center of the resistive touch panel.
    2. Three individual methods that scan the X, Y, and Z components 
    respectively.
        a. The X and Y components should return values using the same units as 
        the width and length specified in the constructor such that the 
        readings are zero if the point of contact is in the middle of the 
        touch panel.
        b. The Z component should return as a Boolean value representing 
        whether or not something is in contact with the touch panel.
    3. A method that reads all three components and returns their values as a 
    tuple.
    4. You must be able to read from a single channel in less than 500μs and 
    all three channels in less than 1500μs to guarantee that your driver works 
    cooperatively with other code you will run for your project. If you 
    optimize your code properly you should be able to get the time to measure 
    all three channels down to less than 500μs.
    5. An optional, but very beneficial feature would be an additional method 
    that allows you to calibrate your sensor by touching the screen in 
    multiple locations and computing the horizontal and vertical scaling and 
    center location automatically.
    
@section sec_DCM DC Motor Driver Requirements
    
    1. Define two classes: Motor Driver IC and Motor connected to driver IC
    2. Driver must independently interact with each motor
    3. Integrate Fault Detection Components (see requirements in Fault 
    Detection Section, DCMotorsFD.py) \ref
    
@section sec_ENCDRV Encoder Driver Requirements

    1. Write a class which encapsulates the operation of the timer to read 
    from an encoder connected to arbitrary pins.
    2. A user should be able to create at least two objects of this class on 
    different pins and different timers; ideally, the user would be able to 
    specify any valid pin and timer combination to create many encoder objects. 
    These two objects must be able to work at the same time without them 
    interfering with one another.
    3. Your code must work if the timer overflows (counts above 216 − 1) or 
    underflows (counts below zero).
    4. The position should count total movement and not reset for each 
    revolution or when the timer overflows.
    
@section sec_file File List
@ref Touch Panel Driver: TouchPanelDriver.py
@ref Motor Driver: DRV8847.py
@ref Encoder Driver: Encoder_Driver.py
"""